import React, { Component } from 'react';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as companyActions from '../../redux/actions/companyActions';
import { apiURL } from '../../common/global';
import { activityType } from '../../common/enum';

const actions = [
    companyActions
]

class People extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshCount: props.refreshCount,
            activeCompany: props.activeCompany,
            // energy: props.activeCompany.energyScore,
            // trust: props.activeCompany.trustScore,
            // seat: props.activeCompany.seatScore,
            // companyScore: props.activeCompany.score,
            loginUser: props.user.user,
        }
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.refreshCount != props.refreshCount && this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany, refreshCount: props.refreshCount }, () => {
                //this.loadData();
            });
        }
        if (this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany });
        }
        if (this.state.energy != props.company.energyScore) {
            this.setState({ energy: props.company.energyScore });
        }
        if (this.state.trust != props.company.trustScore) {
            this.setState({ trust: props.company.trustScore });
        }
        if (this.state.seat != props.company.seatScore) {
            this.setState({ seat: props.company.seatScore });
        }
        if (this.state.companyScore != props.company.companyScore) {
            this.setState({ companyScore: props.company.companyScore });
        }
        if (this.state.loginUser != props.user.user) {
            this.setState({ loginUser: props.user.user });
        }
        if (props.company.isCompanyScoreUpated) {
            this.props.actions.clearCompanyScore();
            this.props.actions.getCompany(this.state.loginUser);
        }
    }
    // async loadData() {
    //     const res = await fetch(apiURL + "energy?companyId=" + this.state.activeCompany._id);
    //     const json = await res.json();
    //     if (json.success) {
    //         let energy = json.data;
    //         let etotal, total;
    //         etotal = energy.filter(a => a.type == activityType.energizing).length;
    //         total = energy.length;
    //         let energizing;
    //         energizing = Math.round((etotal / total) * 100);
    //         if (Object.is(energizing, NaN))
    //             energizing = 0;
    //         this.props.actions.updateEnergyScore(energizing);
    //         let companyScroe = Math.round((this.state.trust + this.state.seat + energizing) / 3);
    //         if (Object.is(companyScroe, NaN))
    //             companyScroe = 0;
    //         this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
    //     }
    // }
    render() {
        return (
            <section id="people" className="collapse">
                <div className="menu">
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("people", "cmpmenu")}>{this.state.activeCompany != undefined ? this.state.activeCompany.name : ""}</button>
                    <button className="lik-btn active" type="button">People</button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("people", "strategy")}>Strategy</button>
                    <button className="lik-btn " type="button" onClick={() => this.props.nav("people", "execution")} > Execution </button >
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("people", "numbers")} > Numbers</button >
                </div >
                <div className="people-stats">
                    <div className="container center">
                        <p className="stats-item">No company performs better than the leadership team performs as a team <span
                            className="highlight-badge stats">{this.state.companyScore}%</span></p>
                        <table id="tblpeople-stats">
                            <thead></thead>
                            <tbody>
                                <tr>
                                    <td>Seats</td>
                                    <td><span className="active stats" style={{ cursor: 'pointer' }} onClick={() => this.props.nav("people", "seats")}>{this.state.seat != null && this.state.seat != undefined ? this.state.seat : 0}%</span></td>
                                </tr>
                                <tr>
                                    <td>Energy</td>
                                    <td><span className="active stats" style={{ cursor: 'pointer' }} onClick={() => this.props.nav("people", "energy")}>{this.state.energy}%</span></td>
                                </tr>
                                <tr>
                                    <td>Trust</td>
                                    <td><span className="active stats" style={{ cursor: 'pointer' }} onClick={() => this.props.nav("people", "trust")}>{this.state.trust}%</span></td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                </div>
            </section>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}
export default connect(mapStateToProps, mapDispatchToProps)(People)

