import React, { Component } from 'react';
//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as companyActions from '../../redux/actions/companyActions';
import { apiURL } from '../../common/global';
import { activityType, userType } from '../../common/enum';
const actions = [
    companyActions
]

class Energy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            energyData: [],
            refreshCount: props.refreshCount,
            activeCompany: props.activeCompany,
            currentActivity: undefined,
            currentUserData: [],
            energizing: 0,
            ok: 0,
            draining: 0,
            trustScore: props.company.trustScore,
            seatScore: props.company.seatScore,
            loginUser: props.user.user,

        }
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.refreshCount != props.refreshCount && this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany, refreshCount: props.refreshCount }, () => {
                //Load data for selected company
                $("#tblenergy-data")[0].classList.add("collapse");
                this.loadData();
            });
        }
        if (this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany });
        }
        if (this.state.trustScore != props.company.trustScore) {
            this.setState({ trustScore: props.company.trustScore });
        }
        if (this.state.seatScore != props.company.seatScore) {
            this.setState({ seatScore: props.company.seatScore });
        }
        if (this.state.loginUser != props.user.user) {
            this.setState({ loginUser: props.user.user });
        }
        if (props.company.isCompanyScoreUpated) {
            this.props.actions.clearCompanyScore();
            this.props.actions.getCompany(this.state.loginUser);
        }
    }
    //DB data save
    async loadData() {
        if (this.state.activeCompany == undefined)
            return;

        await this.getAllUser();
        this.getEnergyData();
    }
    async getAllUser() {
        const res = await fetch(apiURL + "user?companyId=" + this.state.activeCompany._id);
        const json = await res.json();
        if (json.success) {
            let users = [];
            if (this.state.loginUser == userType.cAdmin) {
                const loginID = this.state.loginUser._id;
                users = json.data.filter(function (obj) {
                    return obj._id !== loginID;
                });
            }
            else {
                users = json.data;
            }
            if (users.length == 0) {
                $("#top-alert")[0].classList.remove("collapse");
                setTimeout(() => {
                    $("#top-alert")[0].classList.add("collapse");
                }, 5000);
            }
            this.setState({ users: users });
        }
    }
    async getEnergyData(force) {
        const res = await fetch(apiURL + "energy?companyId=" + this.state.activeCompany._id);
        const json = await res.json();
        if (json.success) {
            let energy = json.data;
            let allData = [];
            for (let i = 0; i < this.state.users.length; i++) {
                const user = this.state.users[i];
                let userData = energy.filter(a => a.userId._id == user._id);
                if (userData.length > 0) {
                    let etotal, ototal, dtotal, total;
                    etotal = userData.filter(a => a.type == activityType.energizing).length;
                    ototal = userData.filter(a => a.type == activityType.ok).length;
                    dtotal = userData.filter(a => a.type == activityType.draining).length;
                    total = userData.length;
                    let energizing, ok, draining;
                    energizing = Math.round((etotal / total) * 100);
                    ok = Math.round((ototal / total) * 100);
                    draining = Math.round((dtotal / total) * 100);
                    allData.push({ userId: user, energizing: energizing, ok: ok, draining: draining, data: userData });
                }
                else {
                    allData.push({ userId: user, energizing: 0, ok: 0, draining: 0, data: [] });
                }
            }
            if (this.state.currentActivity != undefined && this.state.currentActivity.userId != undefined) {
                let getSelectedUserData = allData.filter(a => a.userId._id == this.state.currentActivity.userId._id)[0];
                if (getSelectedUserData != undefined) {
                    this.setState({ currentUserData: getSelectedUserData.data });
                }
            }
            let etotal, ototal, dtotal, total;
            etotal = energy.filter(a => a.type == activityType.energizing).length;
            ototal = energy.filter(a => a.type == activityType.ok).length;
            dtotal = energy.filter(a => a.type == activityType.draining).length;
            total = energy.length;
            let energizing, ok, draining;
            energizing = Math.round((etotal / total) * 100);
            ok = Math.round((ototal / total) * 100);
            draining = Math.round((dtotal / total) * 100);
            if (Object.is(energizing, NaN))
                energizing = 0;
            //this.props.actions.updateEnergyScore(energizing);
            if (force) {
                this.updateScoreForEnergy(this.state.activeCompany._id, energizing);
                let companyScroe = Math.round((this.state.trustScore + this.state.seatScore + energizing) / 3);
                if (Object.is(companyScroe, NaN))
                    companyScroe = 0;
                this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
            }
            this.setState({ energyData: allData, energizing: Object.is(energizing, NaN) ? 0 : energizing, ok: Object.is(ok, NaN) ? 0 : ok, draining: Object.is(draining, NaN) ? 0 : draining });
        }
    }
    updateScoreForEnergy(companyId, energyScore) {
        fetch(apiURL + 'companyScore', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                isEnergy: true,
                _id: companyId,
                energyScore: energyScore,
            }),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success)
                    this.props.actions.updateEnergyScore(energyScore);
            })
            .catch((error) => {
                console.log(error);
            });
    }
    async saveEnergy(item) {
        // if (item.activityId == undefined || item.activityName.trim() == "")
        //     return;
        var energy = {
            userId: item.userId._id,
            activity: item.activityName,
            activityId: item.activityId,
            type: item.type,
            description: '',
        }
        fetch(apiURL + 'energy', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(energy),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success) {
                    this.getEnergyData(true);
                    this.setState({ currentActivity: this.initializeBlankActivity(this.state.currentActivity) }, () => {
                        this.scrollToBottom();
                    });
                }
                else {
                    alert(rsp.message);
                }
            })
            .catch((error) => {
                alert("Error" + error);
            });
    }

    //UI Operation
    onTrClick(item, pid) {
        if (this.state.loginUser.userType.includes(userType.teamMember) && this.state.loginUser._id != item.userId._id)
            return;
        $("#tblenergy-data")[0].classList.remove("collapse");
        if (item.data.length == 0) {
            this.setState({ currentUserData: item.data, currentActivity: this.initializeBlankActivity(item) }, () => {
                this.enableActivityAutocomplete();
            });
        }
        else {
            this.setState({ currentUserData: item.data, currentActivity: this.initializeBlankActivity(item) }, () => {
                this.enableActivityAutocomplete();
            });
        }
    }
    onchangeActivityInput(e) {
        let activity = this.state.currentActivity;
        activity.activityName = e.target.value;
        this.setState({ currentActivity: activity });
    }
    onChangeCategory(e) {
        let currentActivity = this.state.currentActivity;
        currentActivity.type = e.target.value;
        this.setState({ currentActivity: currentActivity });
    }
    onChangeCategoryExisting(item, e) {
        item.type = e.target.value;
        fetch(apiURL + 'energy', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(item),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success) {
                    this.getEnergyData(true);
                }
                else {
                    alert(rsp.message);
                }
            })
            .catch((error) => {
                alert("Error" + error);
            });
    }
    onDeleteActivity(item) {
        if (confirm("Are you sure want to delete?")) {
            fetch(apiURL + 'energy', {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: item._id,
                }),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        this.getEnergyData(true);
                    }
                    else {
                        alert(rsp.message);
                    }
                })
                .catch((error) => {
                    alert("Error" + error);
                });
        }
    }
    initializeBlankActivity(item) {
        let newActivity = {
            userId: item.userId,
            activityId: undefined,
            activityName: '',
            type: activityType.energizing,
            description: ''
        }
        return newActivity;
    }
    scrollToBottom = () => {
        this.activityEnd.scrollIntoView({ behavior: "smooth" });
    }
    enableActivityAutocomplete() {
        const changeOnSelect = (event, ui) => {
            event.preventDefault();
            $("#addActivity").val(ui.item.label);
            $("#addActivity").data('id', ui.item.value);
            let currentActivity = this.state.currentActivity;
            currentActivity.activityName = ui.item.label;
            currentActivity.activityId = ui.item.value;
            this.setState({ currentActivity: currentActivity });
        }

        $("#addActivity").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: apiURL + "activityautocomplete?q=" + request.term,
                    success: function (data) {
                        response(data);
                    },
                });
            },
            minLength: 2,
            select: changeOnSelect,
        });
    }
    //Render
    render() {
        return (
            <section id="energy" className="collapse">
                <div className="menu">
                    <button className="img-btn" type="button" style={{ paddingLeft: 10, marginBottom: 20 }} onClick={() => this.props.nav("energy", "cmpmenu")}><img src="img/back-arw.png" /></button>
                    <button className="lik-btn" type="button" onClick={() => {
                        this.props.nav("energy", "people");
                        $("#tblenergy-data")[0].classList.add("collapse");
                    }}>People</button>
                    <button className="lik-btn" type="button" onClick={() => {
                        this.props.nav("energy", "seats")
                        $("#tblenergy-data")[0].classList.add("collapse");
                    }}>Seats</button>
                    <button className="lik-btn active" type="button">Energy</button>
                    <button className="lik-btn" type="button" onClick={() => {
                        this.props.nav("energy", "trust")
                        $("#tblenergy-data")[0].classList.add("collapse");
                    }}>Trust</button>
                </div >
                <div className="energy-Seats">
                    <div className="container">
                        <div className="energy-data">
                            <table id="tblenergy" className="center">
                                <thead>
                                    {/* <tr id="energy-control">
                                        <th>

                                        </th>
                                        <th></th>
                                        <th>

                                        </th>
                                        <th>

                                        </th>
                                    </tr> */}
                                    <tr>
                                        <th>Team</th>
                                        <th>Energizing</th>
                                        <th>OK</th>
                                        <th>Draining</th>
                                        <th className="no-border no-bg">
                                            <button id="selectweeks" className="lik-btn active" type="button" style={{ alignSelf: 'flex-end' }}>Past</button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.energyData.map((item, index) => {
                                            return (
                                                <tr key={index} className="pa" data-pid="1" title="Click on your name to enter activities and classify" onClick={() => this.onTrClick(item, index)}>
                                                    <td>{item.userId.name}</td>
                                                    <td id="e1" className="e-data" data-edata="100">{item.energizing}%</td>
                                                    <td id="o1" className="o-data" data-odata="0">{item.ok}%</td>
                                                    <td id="d1" className="d-data" data-ddata="0">{item.draining}%</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td className="no-border"></td>
                                        <td id="e-total" className="highlight-prime" data-etdata="40">{this.state.energizing}%</td>
                                        <td id="o-total" data-otdata="40">{this.state.ok}%</td>
                                        <td id="d-total" data-dtdata="40">{this.state.draining}%</td>
                                    </tr>
                                    {/* <tr>
                                        <td className="no-border no-padding" colSpan="4">
                                            <p id="hint-energy" className="highlight">Click on your name to enter activities and
                                            classify</p>
                                        </td>
                                    </tr> */}
                                </tfoot>
                            </table>
                            <table id="tblenergy-data" className="collapse">
                                <thead>
                                    <tr>
                                        <th style={{ minWidth: 100 }}>Name</th>
                                        <th>Activity</th>
                                        <th style={{ minWidth: 200, textAlign: 'center' }}>Select Category</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.currentUserData.map((item, index) => {
                                            return (
                                                <tr key={index} className="ar_1 p-activity">
                                                    <td>{item.userId.name}</td>
                                                    <td>{item.activityId.name}</td>
                                                    <td style={{ textAlign: 'center' }}>
                                                        <select className="sev p1 av1" data-pid="1" value={item.type} onChange={(e) => this.onChangeCategoryExisting(item, e)}>
                                                            <option value="1">E</option>
                                                            <option value="2">O</option>
                                                            <option value="3">D</option>
                                                        </select>
                                                    </td>
                                                    <td style={{ textAlign: 'center', cursor: 'pointer' }} title={"Click to delete"} onClick={() => this.onDeleteActivity(item)}><span>X</span></td>
                                                </tr>
                                            )
                                        })
                                    }
                                    {
                                        this.state.currentActivity != undefined ?
                                            <tr className="ar_1 p-activity">
                                                <td>{this.state.currentActivity.userId.name}</td>
                                                <td><input id="addActivity" className="addactivity" value={this.state.currentActivity.activityName} type="text" placeholder="Enter activity" onChange={(value) => this.onchangeActivityInput(value)} /></td>
                                                <td style={{ textAlign: 'center' }}>
                                                    <select className="sev p1 av2222" data-pid="1" value={this.state.currentActivity.type} onChange={(e) => this.onChangeCategory(e)}
                                                        disabled={!(this.state.currentActivity.activityName != undefined && this.state.currentActivity.activityName.trim() != "")}>
                                                        <option value="1">E</option>
                                                        <option value="2">O</option>
                                                        <option value="3">D</option>
                                                    </select>
                                                </td>
                                                <td style={{ textAlign: 'center', cursor: 'pointer' }} title={"Click to save"} onClick={() => this.saveEnergy(this.state.currentActivity)} ><span>✓</span></td>
                                            </tr>
                                            : null
                                    }
                                </tbody>
                            </table>
                            <div style={{ height: 200 }} ref={(el) => { this.activityEnd = el; }} />
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}
export default connect(mapStateToProps, mapDispatchToProps)(Energy)