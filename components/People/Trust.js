import React, { Component } from 'react';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isValueObject, Map } from 'immutable';
import * as companyActions from '../../redux/actions/companyActions';
import { apiURL } from '../../common/global';
import { activityType, userType } from '../../common/enum';
const actions = [
    companyActions
]

const trustRow = [
    "Create Clarity",
    "Put Enterprise First",
    "Demonstrate Character",
    "Strive to Improve",
    "Correct Wrongs",
    "Keep Promises",
    "Deliver Results",
    "Be Consistent"
]

class Trust extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            trustData: [],
            keyValuData: [],
            tPoints: [],
            prevPoints: [],
            prevRated: null,
            tPer: 0,
            energyScore: props.company.energyScore,
            seatScore: props.company.seatScore,
            loginUser: props.user.user,
            sessionDateTime: undefined,
            trustAllow: undefined,
            trustdone: undefined,
        }
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.refreshCount != props.refreshCount && this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany, refreshCount: props.refreshCount }, () => {
                //Load data for selected company
                this.loadData();
            });
        }
        if (this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany });
        }
        if (this.state.energyScore != props.company.energyScore) {
            this.setState({ energyScore: props.company.energyScore });
        }
        if (this.state.seatScore != props.company.seatScore) {
            this.setState({ seatScore: props.company.seatScore });
        }
        if (this.state.loginUser != props.user.user) {
            this.setState({ loginUser: props.user.user });
        }
        if (props.company.isCompanyScoreUpated) {
            this.props.actions.clearCompanyScore();
            this.props.actions.getCompany(this.state.loginUser);
        }
    }

    async getTrustAllow() {
        const res = await fetch(apiURL + "trustallow?companyId=" + this.state.activeCompany._id);
        const json = await res.json();
        let trustAllow = undefined;
        if (json.success) {
            if (json.data != undefined) {
                const resTRDONE = await fetch(apiURL + "trustdone?trustAllowId=" + json.data._id + "&&userId=" + this.state.loginUser._id);
                const jsonTrDone = await resTRDONE.json();
                if (jsonTrDone.success) {
                    if (jsonTrDone.data == undefined)
                        trustAllow = json.data;
                }
            }
        }
        this.setState({ trustAllow: trustAllow });
    }
    async loadData() {
        if (this.state.activeCompany == undefined)
            return;
        await this.getAllUser();
        await this.getTrustAllow();
        await this.getTrustData();
        await this.getTotalPoints();
        await this.getRatedBy();

        this.bindData();
    }
    async bindData() {
        let keyValuData = [];
        for (let i = 0; i < trustRow.length; i++) {
            const trustrow = trustRow[i];
            let data = [];
            for (let j = 0; j < this.state.users.length; j++) {
                const user = this.state.users[j];
                let item = this.state.trustData.filter(a => a.userId._id == user._id && a.type.replace("_", " ").toLowerCase() == trustrow.toLowerCase())[0];
                if (item == undefined)
                    item = { userId: user, type: trustrow, tValue: 0, trustByUserId: this.state.loginUser._id };
                data.push(item);
            }
            keyValuData.push({ key: trustrow, data: data });
        }
        let tPoints = [];
        let totalPoints = 0;
        var tPointPer = 0;
        var sumOfPer = 0;
        var tPer = 0;
        var tesing = 0;
        for (let i = 0; i < this.state.users.length; i++) {
            const user = this.state.users[i];
            let trustOfUser = this.state.trustData.filter(a => a.userId._id == user._id);
            let totalTrustPoint = 0;
            for (let j = 0; j < trustOfUser.length; j++) {
                const trust = trustOfUser[j];
                totalTrustPoint += trust.tValue;
            }
            totalPoints += totalTrustPoint;
            // tPointPer = Math.round((totalTrustPoint / 40) * 100);
            // tPoints.push({ key: user, value: totalTrustPoint, per: tPointPer });
        }
        if (this.state.prevPoints.length > 0) {
            for (let i = 0; i < this.state.prevPoints.length; i++) {
                const user = this.state.prevPoints[i].key
                var term = this.state.prevPoints[i].value;
                var totalNumber = this.state.prevPoints[i].finaltotalNumberOfPointWeGot;
                var test = totalNumber;
                if (test == 0) {
                    tesing += 1;
                }
                tPointPer = Math.round((term / (40 * totalNumber)) * 100);
                if (Object.is(tPointPer, NaN))
                    tPointPer = 0;
                sumOfPer += tPointPer;
                tPoints.push({ key: user, value: term, per: tPointPer });
            }
            if (tesing >= 1) {
                tPer = Math.round((sumOfPer / this.state.users.length));
                if (Object.is(tPer, NaN))
                    tPer = 0;
                let companyScroe = Math.round((tPer + this.state.seatScore + this.state.energyScore) / 3);
                if (Object.is(companyScroe, NaN))
                    companyScroe = 0;
            }
            else {
                tPer = Math.round((sumOfPer / this.state.users.length));
                if (Object.is(tPer, NaN))
                    tPer = 0;
                this.updateScoreForTrust(this.state.activeCompany._id, tPer);
                let companyScroe = Math.round((tPer + this.state.seatScore + this.state.energyScore) / 3);
                if (Object.is(companyScroe, NaN))
                    companyScroe = 0;
                this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
                this.setState({ tPoints: tPoints });
            }
            // tPer = Math.round((sumOfPer / this.state.users.length));
            // if (Object.is(tPer, NaN))
            //     tPer = 0;
            // this.updateScoreForTrust(this.state.activeCompany._id, tPer);
            // let companyScroe = Math.round((tPer + this.state.seatScore + this.state.energyScore) / 3);
            // if (Object.is(companyScroe, NaN))
            //     companyScroe = 0;
            // this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
            // this.setState({ tPoints: tPoints});
        }
        // else{
        //     alert("here");
        //     tPer = Math.round(((totalPoints / this.state.users.length) / 40) * 100);
        //     if (Object.is(tPer, NaN))
        //         tPer = 0; 
        // }
        //this.props.actions.updateTrustScore(tPer);
        // if (force) {
        //     this.updateScoreForTrust(this.state.activeCompany._id, tPer)
        //     let companyScroe = Math.round((tPer + this.state.seatScore + this.state.energyScore) / 3);
        //     if (Object.is(companyScroe, NaN))
        //         companyScroe = 0;
        //     this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
        // }
        this.setState({ keyValuData: keyValuData, tPoints: tPoints, tPer: tPer });
    }

    updateScoreForTrust(companyId, trustScore) {
        fetch(apiURL + 'companyScore', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                isTrust: true,
                _id: companyId,
                trustScore: trustScore,
            }),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success)
                    this.props.actions.updateTrustScore(trustScore);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    refreshTotal() {
        let tPoints = [];
        let totalPoints = 0;
        for (let i = 0; i < this.state.users.length; i++) {
            const user = this.state.users[i];
            let totalTrustPoint = 0;
            for (let j = 0; j < this.state.keyValuData.length; j++) {
                const trust = this.state.keyValuData[j].data[i];
                totalTrustPoint += Number(trust.tValue);
            }
            totalPoints += totalTrustPoint;
            let tPointPer = Math.round((totalTrustPoint / 40) * 100);
            tPoints.push({ key: user, value: totalTrustPoint, per: tPointPer });
        }
        let tPer = Math.round(((totalPoints / this.state.users.length) / 40) * 100);
        if (Object.is(tPer, NaN))
            tPer = 0;
        // this.updateScoreForTrust(this.state.activeCompany._id, tPer);
        // let companyScroe = Math.round((tPer + this.state.seatScore + this.state.energyScore) / 3);
        // if (Object.is(companyScroe, NaN))
        //     companyScroe = 0;
        // this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
        this.setState({ tPoints: tPoints, tPer: tPer });
    }

    newTotal() {
        let tPoints = [];
        let totalPoints = 0;
        for (let i = 0; i < this.state.users.length; i++) {
            const user = this.state.users[i];
            let totalTrustPoint = 0;
            for (let j = 0; j < this.state.keyValuData.length; j++) {
                const trust = this.state.keyValuData[j].data[i];
                totalTrustPoint += Number(trust.tValue);
            }
            totalPoints += totalTrustPoint;
            let tPointPer = Math.round((totalTrustPoint / 40) * 100);
            tPoints.push({ key: user, value: totalTrustPoint, per: tPointPer });
        }
        let tPer = Math.round(((totalPoints / this.state.users.length) / 40) * 100);
        if (Object.is(tPer, NaN))
            tPer = 0;
        this.updateScoreForTrust(this.state.activeCompany._id, tPer);
        let companyScroe = Math.round((tPer + this.state.seatScore + this.state.energyScore) / 3);
        if (Object.is(companyScroe, NaN))
            companyScroe = 0;
        this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
        this.setState({ tPoints: tPoints, tPer: tPer });
    }

    // refresh
    async getAllUser() {
        const res = await fetch(apiURL + "user?companyId=" + this.state.activeCompany._id);
        const json = await res.json();
        if (json.success && json.data.length > 0) {
            const loginID = this.state.loginUser._id;
            let users = json.data.filter(function (obj) {
                return obj._id !== loginID;
            });
            if (users.length == 0) {
                $("#top-alert")[0].classList.remove("collapse");
                setTimeout(() => {
                    $("#top-alert")[0].classList.add("collapse");
                }, 5000);
            }
            this.setState({ users: users });
        }
    }
    async getTrustData(isPast = undefined, currentSessionTime = undefined) {
        const res = await fetch(apiURL + "trust?companyId=" + this.state.activeCompany._id + "&&trustByUserId=" + this.state.loginUser._id + "&&isPast=" + isPast + "&&currentSessionTime=" + currentSessionTime);
        const json = await res.json();
        if (json.success) {
            this.setState({ trustData: json.data });
        }
    }
    async getTotalPoints(isPast = undefined, currentSessionTime = undefined) {
        const res = await fetch(apiURL + "totalpoints?companyId=" + this.state.activeCompany._id + "&&totalPointsByUserID=" + this.state.loginUser._id + "&&isPast=" + isPast + "&&currentSessionTime=" + currentSessionTime + "&&loginType=" + this.state.loginUser.userType);
        const json = await res.json();
        if (json.success) {
            this.setState({ prevPoints: json.data });
        }
    }

    async getRatedBy(isPast = undefined, currentSessionTime = undefined) {
        const res = await fetch(apiURL + "ratedTrust?companyId=" + this.state.activeCompany._id + "&&ratedByUserID=" + this.state.loginUser._id + "&&isPast=" + isPast + "&&currentSessionTime=" + currentSessionTime);
        const json = await res.json();
        if (json.success) {
            this.setState({ prevRated: json.data });
            // alert(json.data);
        }
    }

    onChangeValue(e, cIndex, rIndex, uData) {
        if (this.state.loginUser.userType.includes(userType.cAdmin) || this.state.loginUser.userType.includes(userType.superAdmin) || (this.state.loginUser.userType.includes(userType.teamMember) && this.state.trustAllow != undefined && this.state.trustAllow != null)) {

        }
        else {
            return;
        }
        if (this.validateTValue(e.target.value)) {
            let keyvalData = this.state.keyValuData;
            keyvalData[rIndex].data[cIndex].tValue = e.target.value;
            this.setState({ keyValuData: keyvalData });
            // let valutoSave = keyvalData[rIndex].data[cIndex];
            // valutoSave.userId = valutoSave.userId._id;
            // const trust = valutoSave;

            //if (this.state.loginUser.userType.includes(userType.cAdmin) || this.state.loginUser.userType.includes(userType.superAdmin) || (this.state.loginUser.userType.includes(userType.teamMember) && this.state.trustAllow != undefined && this.state.trustAllow != null))
            this.refreshTotal();
            // this.refreshPer();
            //save
            // fetch(apiURL + 'trust', {
            //     method: 'POST',
            //     headers: {
            //         'Content-Type': 'application/json',
            //     },
            //     body: JSON.stringify(trust),
            // })
            //     .then((response) => response.json())
            //     .then((rsp) => {
            //         if (rsp.success) {
            //             //this.bindData();
            //             this.refreshTotal();
            //         }
            //         else {
            //             alert(rsp.message);
            //         }
            //     })
            //     .catch((error) => {
            //         alert("Error" + error);
            //     });
        }
    }
    loadPastData() {

    }
    clearTrust() {
        let trustClear = {
            _id: this.state.activeCompany._id,
            isActive: true,
        }
        fetch(apiURL + 'trust', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(trustClear),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success) {
                    //this.bindData();
                    //    alert("you can add new trust now");
                }
                else {
                    alert("error");
                }
            })
            .catch((error) => {
                alert("Error");
            });
    }

    setNewData() {
        let sessionDate = new Date();
        let trustAllow = {
            companyId: this.state.activeCompany._id,
            ratingAllowedBy: this.state.loginUser._id,
            isActive: true,
            dateAdded: sessionDate,
        }
        fetch(apiURL + 'trustallow', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(trustAllow),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success) {
                    //this.bindData();
                    let keyValuData = [];
                    for (let i = 0; i < trustRow.length; i++) {
                        const trustrow = trustRow[i];
                        let data = [];
                        for (let j = 0; j < this.state.users.length; j++) {
                            const user = this.state.users[j];
                            let item = { userId: user, type: trustrow, tValue: 0, trustByUserId: this.state.loginUser._id };
                            data.push(item);
                        }
                        keyValuData.push({ key: trustrow, data: data });
                    }
                    this.setState({ prevRated: null });
                    this.setState({ keyValuData: keyValuData, sessionDateTime: sessionDate, trustAllow: trustAllow }, () => {
                        this.clearTrust();
                        this.newTotal();
                    });
                }
                else {
                    alert(rsp.message);
                }
            })
            .catch((error) => {
                alert("Error" + error);
            });
    }

    saveData() {
        let isValidSubmit = true;
        for (let i = 0; i < this.state.keyValuData.length; i++) {
            const keyvalData = this.state.keyValuData[i];
            for (let j = 0; j < keyvalData.data.length; j++) {
                const trustNode = keyvalData.data[j];
                if (trustNode.tValue == 0) {
                    isValidSubmit = false;
                    break;
                }
            }
            if (isValidSubmit == false)
                break;
        }
        if (isValidSubmit) {
            if (this.state.sessionDateTime == undefined) {
                this.state.sessionDateTime = new Date(); //saving first time
                this.setState({ sessionDateTime: this.state.sessionDateTime });
            }
            let trustData = {
                sessionDateTime: this.state.sessionDateTime,
                trustData: this.state.keyValuData,
                companyId: this.state.activeCompany._id,
            }
            fetch(apiURL + 'trust', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(trustData),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        //this.bindData();
                        // if (this.state.loginUser.userType.includes(userType.superAdmin)){
                        //     // this.setState({adminsubmitvisibility: false});
                        //     this.setState(prevState => ({
                        //         adminsubmitvisibility: !prevState.adminsubmitvisibility
                        //     }));
                        // }
                        this.setState({ tablevisibility: false })
                        if (this.state.loginUser.userType.includes(userType.teamMember) || (this.state.loginUser.userType.includes(userType.cAdmin)) || (this.state.loginUser.userType.includes(userType.superAdmin)))
                            this.saveUserTrustRatingDone();
                        this.loadData();
                        alert("The rating has been submitted");
                    }
                    else {
                        alert(rsp.message);
                    }
                })
                .catch((error) => {
                    alert("Error" + error);
                });
        }
        else {
            alert("Please rate all team member.")
        }
    }

    saveUserTrustRatingDone() {
        if (this.state.trustAllow == undefined || this.state.trustAllow == null) {
            var trustdone = {
                ratedBy: this.state.loginUser._id,
                isActive: true,
            }
        }
        else {
            var trustdone = {
                trustAllowId: this.state.trustAllow._id,
                ratedBy: this.state.loginUser._id,
                isActive: true,
            }
        }
        // let trustdone = {
        //     trustAllowId: this.state.trustAllow._id,
        //     ratedBy: this.state.loginUser._id,
        //     isActive: true,
        // }
        fetch(apiURL + 'trustdone', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(trustdone),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success) {
                    this.setState({ trustdone: trustdone, submitvisibility: true });
                }
                else {
                    alert(rsp.message);
                }
            })
            .catch((error) => {
                alert("Error" + error);
            });
    }
    onFocusCell(cIndex, rIndex, uData) {
        let keyvalData = this.state.keyValuData;
        if (keyvalData[rIndex].data[cIndex].tValue == 0) {
            keyvalData[rIndex].data[cIndex].tValue = '';
            this.setState({ keyValuData: keyvalData });
        }
    }
    onBlurCell(cIndex, rIndex, uData) {
        let keyvalData = this.state.keyValuData;
        if (keyvalData[rIndex].data[cIndex].tValue == '') {
            keyvalData[rIndex].data[cIndex].tValue = 0;
            this.setState({ keyValuData: keyvalData });
        }
    }
    validateTValue(value) {
        var re = /^[0-5]*$/;
        return re.test(value);
    };
    render() {
        return (
            <section id="trust" className="collapse">
                <div className="menu">
                    <button className="img-btn" type="button" style={{ paddingLeft: 10, marginBottom: 20 }} onClick={() => this.props.nav("trust", "cmpmenu")}><img src="img/back-arw.png" /></button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("trust", "people")}>People</button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("trust", "seats")}>Seats</button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("trust", "energy")}> Energy</button >
                    <button className="lik-btn active" type="button">Trust</button>
                </div>
                <div>
                    <div className="trust-data" className="center">
                        <table className="centerTable">
                            <td>
                                <table id="tbltrust" className="center" >
                                    <thead>
                                        <tr>
                                            <th>Rate 1 - 5</th>
                                            {
                                                this.state.users.map((item, index) => {
                                                    return (<th key={index}>{item.name}</th>)
                                                })
                                            }
                                            <th className="no-border no-bg" rowSpan="3">

                                            </th>
                                            <th className="no-border no-bg">

                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.keyValuData.map((item, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>{item.key}</td>
                                                        {
                                                            item.data.map((uData, uIndex) => {
                                                                return (<td key={uIndex} className="tbledit">{((this.state.loginUser.userType.includes(userType.teamMember) && this.state.trustAllow != undefined && this.state.trustAllow != null) || (this.state.loginUser.userType.includes(userType.superAdmin) && this.state.trustAllow != undefined && this.state.trustAllow != null && this.state.prevRated == null || this.state.prevRated == undefined) || (this.state.loginUser.userType.includes(userType.cAdmin) && this.state.trustAllow != undefined && this.state.trustAllow != null && this.state.prevRated == null || this.state.prevRated == undefined)) ? <input className={"ptv pt" + uIndex} data-pt={uIndex} type="text" maxLength="1" value={uData.tValue} onBlur={() => this.onBlurCell(uIndex, index, uData)} onFocus={() => this.onFocusCell(uIndex, index, uData)} onChange={(e) => { this.onChangeValue(e, uIndex, index, uData) }} /> : <input className={"ptv pt" + uIndex} data-pt={uIndex} type="text" maxLength="1" value={uData.tValue} editable={false} />}</td>)
                                                            })
                                                        }
                                                    </tr>

                                                )
                                            })
                                        }
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colSpan="1" id="e-total" data-etdata="40">Total points</td>
                                            {
                                                this.state.tPoints.map((item, index) => {
                                                    return (<td key={index} id={"ptotal" + (index + 1)} data-pt={(index + 1)}>{item.value}</td>)
                                                })
                                            }
                                        </tr>
                                        <tr>
                                            <td id="ptotalpecavg" className="highlight-prime">{this.state.tPer}%</td>
                                            {
                                                this.state.tPoints.map((item, index) => {
                                                    return (<td key={index} id={"ptotal" + (index + 1)} data-pt={(index + 1)}>{item.per}%</td>)
                                                })
                                            }
                                            {/* <td className="no-border no-bg">
                                        <button id="selectweeks" className="lik-btn active" type="button" onClick={() => this.saveData()}>Submit</button>
                                    </td> */}
                                        </tr>
                                    </tfoot>
                                </table>
                            </td>
                            <td>
                                <div style={{ display: 'block' }}>
                                    <div>
                                        {this.state.loginUser.userType.includes(userType.cAdmin) || this.state.loginUser.userType.includes(userType.superAdmin) ?
                                            <button id="selectweeks" className="lik-btn active" type="button" onClick={() => this.setNewData()}>New</button>
                                            : null}
                                    </div>
                                    <div>
                                        {
                                            (this.state.loginUser.userType.includes(userType.teamMember) && this.state.trustAllow != undefined && this.state.trustAllow != null && this.state.trustdone == null && this.state.trustdone == undefined) ||
                                                // (this.state.loginUser.userType.includes(userType.superAdmin) && this.state.trustAllow != undefined && this.state.trustAllow != null && this.state.trustdone == null && this.state.trustdone == undefined )||
                                                // (this.state.loginUser.userType.includes(userType.cAdmin) && this.state.trustAllow != undefined && this.state.trustAllow != null && this.state.trustdone == null && this.state.trustdone == undefined)?
                                                (this.state.loginUser.userType.includes(userType.superAdmin) && this.state.trustAllow != undefined && this.state.trustAllow != null && this.state.prevRated == null) ||
                                                (this.state.loginUser.userType.includes(userType.cAdmin) && this.state.trustAllow != undefined && this.state.trustAllow != null && this.state.prevRated == null) ?
                                                <button id="selectweeks" className="lik-btn active" type="button" onClick={() => this.saveData()}>Submit</button> : null}
                                    </div>
                                    <div>
                                        {this.state.loginUser.userType.includes(userType.cAdmin) || this.state.loginUser.userType.includes(userType.superAdmin) ?
                                            <button id="selectweeks" className="lik-btn active" type="button" onClick={() => this.loadPastData()}>Past</button> : null}
                                    </div>
                                </div>
                            </td>
                        </table>
                    </div>
                </div>
            </section>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}
export default connect(mapStateToProps, mapDispatchToProps)(Trust)