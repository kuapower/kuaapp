import React, { Component } from 'react';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as companyActions from '../../redux/actions/companyActions';
import { apiURL } from '../../common/global';
import { sgrade, userType } from '../../common/enum';

const actions = [
    companyActions
]

class Seats extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            refreshCount: props.refreshCount,
            activeCompany: props.activeCompany,
            keyValuData: [],
            aper: 0,
            currentUserRating: undefined,
            selectedSeat: undefined,
            trustScore: props.company.trustScore,
            energyScore: props.company.energyScore,
            loginUser: props.user.user,
        }
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.refreshCount != props.refreshCount && this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany, refreshCount: props.refreshCount }, () => {
                //Load data for selected company
                this.loadData();
            });
        }
        if (this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany });
        }
        if (this.state.trustScore != props.company.trustScore) {
            this.setState({ trustScore: props.company.trustScore });
        }
        if (this.state.energyScore != props.company.energyScore) {
            this.setState({ energyScore: props.company.energyScore });
        }
        if (this.state.loginUser != props.user.user) {
            this.setState({ loginUser: props.user.user });
        }
        if (props.company.isCompanyScoreUpated) {
            this.props.actions.clearCompanyScore();
            this.props.actions.getCompany(this.state.loginUser);
        }
    }

    async getAllUser() {
        const res = await fetch(apiURL + "user?companyId=" + this.state.activeCompany._id);
        const json = await res.json();
        if (json.success) {
            if (json.data.length == 0) {
                $("#top-alert")[0].classList.remove("collapse");
                setTimeout(() => {
                    $("#top-alert")[0].classList.add("collapse");
                }, 5000);
            }
            this.setState({ users: json.data });
        }
    }
    updateScoreForSeats(companyId, seatScore) {
        fetch(apiURL + 'companyScore', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                isSeat: true,
                _id: companyId,
                seatScore: seatScore,
            }),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success)
                    this.props.actions.updateSeatScore(seatScore);
            })
            .catch((error) => {
                console.log(error);
            });
    }
    async loadData(force) {
        if (this.state.activeCompany == undefined)
            return;

        await this.getAllUser();

        const res = await fetch(apiURL + "seat?companyId=" + this.state.activeCompany._id);
        const json = await res.json();
        if (json.success) {
            let seats = json.data;
            if (force) {
                if (seats.length > 0) {
                    //this.props.actions.updateSeatScore(seats[0].aper);
                    this.updateScoreForSeats(this.state.activeCompany._id, seats[0].aper);
                    let companyScroe = Math.round((this.state.trustScore + seats[0].aper + this.state.energyScore) / 3);
                    if (Object.is(companyScroe, NaN))
                        companyScroe = 0;
                    this.props.actions.updateCompanyScore(this.state.activeCompany._id, companyScroe);
                }
                else {
                    this.updateScoreForSeats(this.state.activeCompany._id, 0);
                    //this.props.actions.updateCompanyScore(this.state.activeCompany._id, 0);
                }
            }
            let orgData = null;
            if (seats.length == 0) {
                orgData = [{
                    id: 1,
                    _id: undefined,
                    userId: undefined,
                    name: "Enter name",
                    position: 'Enter position',
                    description: 'Details here',
                    parent: 0,
                    aper: 0,
                    pObjectId: undefined,
                    plusEnable: !this.state.loginUser.userType.includes(userType.teamMember),
                    deleteEnable: !this.state.loginUser.userType.includes(userType.teamMember)
                }];
            }
            else {
                orgData = seats.map((v, index) => ({
                    id: (index + 1),
                    _id: v._id,
                    userId: v.userId != undefined ? v.userId._id : undefined,
                    name: v.userId != undefined ? v.userId.name : "Enter name",
                    position: v.position,
                    description: v.description,
                    parent: v.parent,
                    aper: v.aper,
                    pObjectId: undefined,
                    plusEnable: !this.state.loginUser.userType.includes(userType.teamMember) || (this.state.loginUser.userType.includes(userType.teamMember) && v.parent == 2 && this.state.loginUser._id == v.userId._id),
                    deleteEnable: !this.state.loginUser.userType.includes(userType.teamMember),
                }));
            }


            // var orgData = [
            //     { id: 1, name: 'Adam', position: 'Visionary', description: 'Accountabilities with KPIs', parent: 0 },
            //     { id: 2, name: 'Jeff', position: 'Integrator', description: 'Accountabilities with KPIs', parent: 1 },
            //     { id: 3, name: 'Mark', position: 'Marketing', description: 'Accountabilities with KPIs', parent: 2 },
            //     { id: 4, name: 'Peter', position: 'Sales', description: 'Accountabilities with KPIs', parent: 2 },
            //     { id: 6, name: 'James', position: 'Operations', description: 'Accountabilities with KPIs', parent: 2 },
            //     { id: 7, name: 'Heli ', position: 'Finance', description: 'Accountabilities with KPIs', parent: 2 },


            // ];

            const activeCompanyId = this.state.activeCompany._id;

            const createNode = (data) => {
                var seat = undefined;
                if (data._id == undefined) {
                    var seat = {
                        companyId: activeCompanyId,
                        position: data.position,
                        userId: data.userId,
                        description: data.description,
                        parent: data.parent,
                        parentSeatId: data.pObjectId
                    };
                }
                else {
                    var seat = {
                        _id: data._id,
                        companyId: activeCompanyId,
                        position: data.position,
                        userId: data.userId,
                        description: data.description,
                        parent: data.parent,
                        parentSeatId: data.pObjectId
                    };
                }
                fetch(apiURL + 'seat', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(seat),
                })
                    .then((response) => response.json())
                    .then((rsp) => {
                        if (rsp.success) {
                            this.loadData(true);
                        }
                        else {
                            alert(rsp.message);
                        }
                    })
                    .catch((error) => {
                        alert("Error" + error);
                    });
            }
            const editnode = (node) => {
                var seat = undefined;
                if (node.data._id == undefined) {
                    var seat = {
                        companyId: activeCompanyId,
                        position: node.data.position,
                        userId: node.data.userId,
                        description: node.data.description,
                        parent: node.data.parent,
                        parentSeatId: node.data.pObjectId
                    };
                }
                else {
                    var seat = {
                        _id: node.data._id,
                        companyId: activeCompanyId,
                        position: node.data.position,
                        userId: node.data.userId,
                        description: node.data.description,
                        parent: node.data.parent,
                        parentSeatId: node.data.pObjectId
                    };
                }
                fetch(apiURL + 'seat', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(seat),
                })
                    .then((response) => response.json())
                    .then((rsp) => {
                        if (rsp.success) {
                            //this.loadData();
                        }
                        else {
                            alert(rsp.message);
                        }
                    })
                    .catch((error) => {
                        alert("Error" + error);
                    });
            }
            const deletenode = (node) => {
                if (node.data.parent > 0) {
                    if (confirm("Are you sure want to delete?")) {
                        org_chart.deleteNode(node.data.id);
                        if (node.data._id != undefined) {
                            fetch(apiURL + 'seat', {
                                method: 'DELETE',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    id: node.data._id,
                                }),
                            })
                                .then((response) => response.json())
                                .then((rsp) => {
                                    if (rsp.success) {
                                        $("#tblseat")[0].classList.add("collapse");
                                        this.loadData(true);
                                    }
                                    else {
                                        alert(rsp.message);
                                    }
                                })
                                .catch((error) => {
                                    alert("Error" + error);
                                });
                        }
                    }
                }
            }
            const addnode = (node) => {
                if (node.data.parent < 2) {
                    org_chart.newNode(node.data.id, node.data._id);
                }
                else {
                    $("#tblseat")[0].classList.remove("collapse");
                    //seat data
                    this.setState({ selectedSeat: node.data });
                    this.loadSeatPerData(node.data._id);
                }
            }
            var org_chart = $('#orgChart').orgChart({
                data: orgData,
                showControls: true,
                allowEdit: true,
                onAddNode: addnode,
                onDeleteNode: deletenode,
                onClickNode: function (node) {
                },
                onEditEnd: editnode,
                onCreateNodeEnd: createNode
            });
        }
    }

    loadSeatPerData(seatId) {
        fetch(apiURL + "seatper?seatId=" + seatId)
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success) {
                    let seatPerData = rsp.data;
                    this.loadData(true);
                    let aGrade = 0;
                    let aGradeCol = seatPerData.filter(a => a.grade == sgrade.A);
                    aGradeCol.forEach(seatper => {
                        aGrade += seatper.grade;
                    });

                    let aper = Math.round((aGrade / seatPerData.length) * 100);
                    if (Object.is(aper, NaN))
                        aper = 0;
                    this.setState({ currentUserRating: this.initialiseUserRatingNewRow(seatId), keyValuData: seatPerData, aper: aper });
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    initialiseUserRatingNewRow(seatId) {
        let seatPer = {
            seatId: seatId,
            username: '',
            grade: 0,
        }
        return seatPer;
    }

    saveSeatPer(item) {
        let seatPer = {
            seatId: item.seatId,
            username: item.username,
            grade: item.grade
        }
        if (item.id != undefined) {
            seatPer._id = item.id;
        }
        fetch(apiURL + 'seatper', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(seatPer),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success) {
                    this.loadSeatPerData(item.seatId);
                }
                else {
                    alert(rsp.message);
                }
            })
            .catch((error) => {
                alert("Error" + error);
            });
    }
    onDeleteSeatPer(item) {
        if (confirm("Are you sure want to delete?")) {
            fetch(apiURL + 'seatper', {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: item._id,
                }),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        this.loadSeatPerData(item.seatId);
                    }
                    else {
                        alert(rsp.message);
                    }
                })
                .catch((error) => {
                    alert("Error" + error);
                });
        }
    }

    onChangeGradeValue(index, item, e) {
        let keyValuData = this.state.keyValuData;
        keyValuData[index].grade = e.target.value;
        this.setState({ keyValuData: keyValuData }, () => {
            //save Seat Per
            let seatPer = {
                seatId: item.seatId,
                userId: item.username,
                grade: item.grade
            }
            if (item._id != undefined) {
                seatPer._id = item._id;
            }
            fetch(apiURL + 'seatper', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(seatPer),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        this.loadSeatPerData(item.seatId);
                    }
                    else {
                        alert(rsp.message);
                    }
                })
                .catch((error) => {
                    alert("Error" + error);
                });
        });
    }
    onClickChartDiv() {
        $("#tblseat")[0].classList.add("collapse");
    }
    render() {
        return (
            <section id="seats" className="collapse">
                {/* <p className="center">Does each person know why they get paid?</p> */}
                <div className="menu">
                    <button className="img-btn" type="button" style={{ paddingLeft: 10, marginBottom: 20 }} onClick={() => this.props.nav("seats", "cmpmenu")}><img src="img/back-arw.png" /></button>
                    <button className="lik-btn" type="button" onClick={() => {
                        $("#tblseat")[0].classList.add("collapse");
                        this.loadData(true);
                        this.props.nav("seats", "people")
                    }}>People</button>
                    <button className="lik-btn active" type="button">Seats</button>
                    <button className="lik-btn" type="button" onClick={() => {
                        $("#tblseat")[0].classList.add("collapse");
                        this.loadData(true);
                        this.props.nav("seats", "energy")
                    }}>Energy</button>
                    <button className="lik-btn" type="button" onClick={() => {
                        $("#tblseat")[0].classList.add("collapse");
                        this.loadData(true);
                        this.props.nav("seats", "trust")
                    }}>Trust</button>
                </div>
                <div id="orgChartContainer" onClick={() => this.onClickChartDiv()}>
                    <div id="orgChart"></div>
                </div>
                <div className="energy-Seats">
                    <div className="container">
                        <div className="energy-data">
                            <table id="tblseat" className="center collapse" style={{ minWidth: 300 }}>
                                <thead>
                                    {/* <tr>
                                        <td colSpan="3" id="e-total" data-etdata="40">{this.state.selectedSeat != undefined ? this.state.selectedSeat.position : ""}</td>
                                    </tr> */}
                                    <tr>
                                        <th style={{ minWidth: 200 }}>Team {this.state.selectedSeat != undefined ? this.state.selectedSeat.position : ""}</th>
                                        <th style={{ minWidth: 200 }}>Rating</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.keyValuData.map((item, index) => {
                                            return (
                                                <tr key={index} className="ar_1 p-activity">
                                                    <td>{item.username}</td>
                                                    <td>
                                                        <select className="sev p1 av1" data-pid="1" value={item.grade} onChange={(e) => this.onChangeGradeValue(index, item, e)}>
                                                            <option value="0">Select Rating</option>
                                                            <option value="1">A</option>
                                                            <option value="2">B</option>
                                                            <option value="3">C</option>
                                                            <option value="4">tA</option>
                                                        </select>
                                                    </td>
                                                    <td style={{ textAlign: 'center', cursor: 'pointer' }} title={"Click to delete"} onClick={() => this.onDeleteSeatPer(item)}><span>X</span></td>
                                                </tr>
                                            )
                                        })
                                    }
                                    {
                                        this.state.currentUserRating != undefined ?
                                            <tr className="ar_1 p-activity">
                                                <td>
                                                    <input className="form-control" type="text" placeholder="Name" value={this.state.currentUserRating.username} onChange={(e) => {
                                                        let currentUserRating = this.state.currentUserRating;
                                                        currentUserRating.username = e.target.value;
                                                        this.setState({ currentUserRating: currentUserRating });
                                                    }} /></td>
                                                <td>
                                                    <select className="sev p1 av1" data-pid="1" disabled={!(this.state.currentUserRating.username != undefined && this.state.currentUserRating.username.trim() != "")} value={this.state.currentUserRating.grade} onChange={(e) => {
                                                        let currentUserRating = this.state.currentUserRating;
                                                        currentUserRating.grade = e.target.value;
                                                        this.setState({ currentUserRating: currentUserRating });
                                                    }}>
                                                        <option value="0">Select Rating</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                        <option value="3">C</option>
                                                        <option value="4">tA</option>
                                                    </select>
                                                </td>
                                                <td style={{ textAlign: 'center', cursor: 'pointer' }} title={"Click to save"} onClick={() => this.saveSeatPer(this.state.currentUserRating)} ><span>✓</span></td>
                                            </tr>
                                            : null
                                    }
                                </tbody>
                                <tfoot>

                                    <tr>
                                        {/* <td colSpan="1" id="e-total" data-etdata="40"></td> */}
                                        <td colSpan="2" id="e-total" data-etdata="40">A-Players</td>
                                        <td id="pseattotal" data-pt={1}>{this.state.aper}%</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div style={{ height: 200 }} ref={(el) => { this.activityEnd = el; }} />
            </section>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}
export default connect(mapStateToProps, mapDispatchToProps)(Seats)

