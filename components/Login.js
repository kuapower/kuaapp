import React, { Component } from 'react';
//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as userActions from '../redux/actions/userActions';
import { apiURL } from '../common/global';

const actions = [
    userActions
]

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            emailTryIt: "",
            emailQuery: "",
            queryText: ""
        }
    }
    handleChange(e) {
        const { id, value } = e.target
        this.setState(prevState => ({
            ...prevState,
            [id]: value
        }))
    }
    onLogin() {
        let isNotSubmit = false, errorMessage = '';
        if (this.state.email == '' || this.state.email == null || this.state.email == undefined) {
            errorMessage = "Enter email.\n";
            isNotSubmit = true;
        }
        else {
            if (!this.validateEmail(this.state.email)) {
                errorMessage = "Enter valid email.\n"
                isNotSubmit = true;
            }
        }
        if (this.state.password == '' || this.state.password == null || this.state.password == undefined) {
            errorMessage = "Enter password.\n";
            isNotSubmit = true;
        }
        if (!isNotSubmit) {
            this.props.actions.login(this.state.email, this.state.password);
        }
        else {
            alert(errorMessage)
        }
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    onTryIt() {
        let isNotSubmit = false, errorMessage = '';
        if (this.state.emailTryIt == '' || this.state.emailTryIt == null || this.state.emailTryIt == undefined) {
            errorMessage = "Enter email.\n";
            isNotSubmit = true;
        }
        else {
            if (!this.validateEmail(this.state.emailTryIt)) {
                errorMessage = "Enter valid email.\n"
                isNotSubmit = true;
            }
        }
        if (!isNotSubmit) {
            fetch(apiURL + 'tryquery', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: this.state.emailTryIt
                }),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        this.setState({ emailTryIt: '' });
                        alert("Thank you for your message");
                    }
                })
                .catch((error) => {
                });
        }
        else {
            alert(errorMessage);
        }
    }
    onSendQuery() {
        let isNotSubmit = false, errorMessage = '';
        if (this.state.emailQuery == '' || this.state.emailQuery == null || this.state.emailQuery == undefined) {
            errorMessage = "Enter email.\n";
            isNotSubmit = true;
        }
        else {
            if (!this.validateEmail(this.state.emailQuery)) {
                errorMessage = "Enter valid email.\n"
                isNotSubmit = true;
            }
        }
        if (this.state.queryText == '' || this.state.queryText == null || this.state.queryText == undefined) {
            errorMessage = "Enter your query.\n";
            isNotSubmit = true;
        }
        if (!isNotSubmit) {
            fetch(apiURL + 'tryquery', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: this.state.emailQuery,
                    query: this.state.queryText
                }),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        this.setState({ emailQuery: '', queryText: '' });
                        alert("Thank you for your message");
                    }
                })
                .catch((error) => {

                });
        }
        else {
            alert(errorMessage);
        }
    }
    render() {
        return (
            <div id="login" className="">
                <div className="center">
                    <p id="main-title" className="h1">
                        Business owners have too much demand on their time and too much information to absorb
                    <br />
                    KUA offers a simple tool to cut through to the essential
                    </p>
                    <p className="h1 h1Header" >Business Decoded</p>
                    <img id="arw" src="img/arw.png" />
                    <div className="accordion" id="login-acc">
                        <div className="signin-sec row">
                            <div id="headingOne" className="col-md-6">
                                <button className="lik-btn" type="button" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">Sign in</button>
                            </div>
                            <div id="collapseOne" className="collapse detail-sec col-md-6" aria-labelledby="headingOne"
                                data-parent="#login-acc">
                                <div className="form-group">
                                    <input id="email" className="form-control" type="text" placeholder="Email" value={this.state.email} onChange={(e) => this.handleChange(e)} />
                                </div>
                                <div className="form-group">
                                    <input id="password" className="form-control" type="password" placeholder="Password" value={this.state.password} onChange={(e) => this.handleChange(e)} />
                                </div>
                                <div className="form-group">
                                    <button className="form-control" type="button" onClick={() => this.onLogin()}>Go</button>
                                </div>
                            </div>
                        </div>
                        <div className="signup-sec row">
                            <div id="headingTwo" className="col-md-6">
                                <button className="lik-btn" type="button" data-toggle="collapse" data-target="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                    Try it
                        </button>
                            </div>
                            <div id="collapseTwo" className="collapse  detail-sec col-md-6" aria-labelledby="headingTwo"
                                data-parent="#login-acc">
                                <div className="form-group">
                                    <input id="emailTryIt" className="form-control" type="text" placeholder="Email" value={this.state.emailTryIt} onChange={(e) => this.handleChange(e)} />
                                </div>
                                <div className="form-group">
                                    <input className="form-control" type="button" value="Join" onClick={() => this.onTryIt()} />
                                </div>
                            </div>
                        </div>
                        <div className="help-sec row">
                            <div id="headingThree" className="col-md-6">
                                <button className="lik-btn" type="button" data-toggle="collapse" data-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                    Ask
                        </button>
                            </div>
                            <div id="collapseThree" className="collapse  detail-sec col-md-6" aria-labelledby="headingThree"
                                data-parent="#login-acc">
                                <div className="form-group">
                                    <input id="emailQuery" className="form-control" type="text" placeholder="Email" value={this.state.emailQuery} onChange={(e) => this.handleChange(e)} />
                                </div>
                                <div className="form-group">
                                    <input id="queryText" className="form-control" type="text" placeholder="Enter your query" value={this.state.queryText} onChange={(e) => this.handleChange(e)} />
                                </div>
                                <div className="form-group">
                                    <input className="form-control" type="button" value="Send" onClick={() => this.onSendQuery()} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)