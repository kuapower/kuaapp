import React, { Component } from 'react';
import { apiURL } from '../../common/global';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';

const actions = [

];

class Strategy extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
    }
    render() {
        return (
            <section id="strategy" className="collapse">
            <div className="menu">
                <button className="lik-btn" type="button" onClick={() => this.props.nav("strategy","people")}>People</button>
                <button className="lik-btn active" type="button">Strategy</button>
                <button className="lik-btn" type="button" onClick={() => this.props.nav("strategy","execution")}>Execution </button>
                <button className="lik-btn" type="button" onClick={() => this.props.nav("strategy","numbers")}>Numbers</button>
            </div>
            <div>
                <div className="container center">
                    <table id="tblstrategy-qa">
                        <thead>
                            
                        </thead>
                        <tbody>
                        <tr>
                            <td className="strategy-que">What's the key problem we are solving?</td>
                            <td id="small-h" className="strategy-ans"><input type="text" placeholder="" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td className="strategy-que">
                                <span id="title"> Most valuable client profile</span>
                                <div className="strategy-ans">
                                    <div id="cimg-section">
    
                                        <div className="circular-sb">
                                            <span> What matters?</span>
                                            <input id="matterans1" type="text" />
                                            <input id="matterans2" type="text" />
                                            <input id="matterans3" type="text" />
                                            <div className="circle1"></div>
                                            <div className="circle2"></div>
                                        </div>
    
                                        <img id="cimg" title="Client image" src="img/client.jpg" />
                                        <button id="btnchoose" className="form-control" type="button">Choose file</button>
                                    </div>
                                </div>
                            </td>
    
                        </tr>
    
                        <tr>
                            <td className="strategy-que whatdiff">
                                What makes us different?
                                 
                                <div id="diffeus" className="strategy-ans">
                                    <span>1 <input id="ans4" type="text" placeholder="Enter your answer"
                                            /></span>
                                    <span>2 <input id="ans5" type="text" placeholder="Enter your answer"
                                          /></span>
                                    <span>3 <input id="ans6" type="text" placeholder="Enter your answer"
                                            /></span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
    
            </div>
        </section>
        )
    }
}


//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Strategy)
