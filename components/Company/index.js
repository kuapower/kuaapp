import React, { Component } from 'react';
import { apiURL } from '../../common/global';
import Modal, { ModalHeader, ModalBody, ModalFooter } from '../../common/Model';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as companyActions from '../../redux/actions/companyActions';
import * as userActions from '../../redux/actions/userActions';
import { userType } from '../../common/enum';

const actions = [
    companyActions,
    userActions
]

class Company extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginUser: props.user.user,
            isAddCompany: false,
            companies: [],
            companyName: '',
            companyHovered: undefined,
        }
        this.toggle = this.toggle.bind(this);
    }
    componentDidMount() {
        this.getData();
    }
    mouseMove(rIndex) {
        this.setState({ companyHovered: rIndex });
    }
    async getData() {
        this.props.actions.getCompany(this.state.loginUser);
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.companies != props.company.companies) {
            this.setState({ companies: props.company.companies });
        }
        if (this.state.loginUser != props.user.user) {
            this.setState({ loginUser: props.user.user });
        }
    }
    handleChange(e) {
        const { id, value } = e.target
        this.setState(prevState => ({
            ...prevState,
            [id]: value
        }))
    }
    toggle() {
        this.setState({ isAddCompany: !this.state.isAddCompany });
    }
    addCompany() {
        this.setState({ isAddCompany: true });
    }
    handleClose = () => {
        this.setState({ isAddCompany: false });
    }
    refresh() {
        this.getData();
        this.props.onRefresh();
    }
    save() {
        if (this.state.companyName.trim() != '' && this.state.companyName.trim() != undefined && this.state.companyName.trim() != null) {
            fetch(apiURL + 'company', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: this.state.companyName
                }),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    this.setState({ companyName: '' });
                    this.refresh();
                })
                .catch((error) => {

                });
        }
        else {
            alert("Enter company name");
        }
    }
    deleteCompany(item) {
        if (confirm("Are you sure want to delete?")) {
            fetch(apiURL + 'company', {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: item._id
                }),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    this.getData();
                })
                .catch((error) => {

                });
        }
    }
    logoutClick() {
        this.props.actions.logout();
    }
    onClickCompany(item) {
        localStorage.setItem('companyId', item._id);
        this.props.nav("company", "cmpmenu", { type: 'company', value: item })
    }
    // adminlogic(){
    //     if (this.state.loginUser.userType == 1) {
    //         return "Main Admin";
    //     } else if(this.state.loginUser.userType == 2){
    //         return "Company Admin";
    //     }
    //     else{
    //         return "Team Member";
    //     }
    // }

    render() {
        return (
            <>
            {/* <div style={{background:"black",padding:"20px"}}>
                <text className="lik-btn-login" type="button" style={{textAlign: 'center',padding:"10px"}}>LOGGED IN:{this.state.loginUser.email} as {this.adminlogic()}</text>
            </div> */}
            {/* <button className="lik-btn" type="button" onClick={() => this.logoutClick()}>Sign out</button> */}
            <section id="company">
                {/* <div className="menuRight">
                    <button className="img-btn" type="button" onClick={() => this.props.nav("user", "company")}><img src="img/back-arw.png" /></button>
                </div> */}
                
                <div className="menu">
                    {this.state.loginUser.userType.includes(userType.superAdmin) ?
                        <button className="lik-btn" type="button" onClick={() => this.addCompany()}>Add Company</button> : null}  
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("company", "user")}>Teams</button>
                    <button className="lik-btn" type="button" onClick={() => this.logoutClick()}>Sign out</button>
                </div>

                <div className="company-action">
                    <div className="container center">
                        <div className="cmplist" id="login-acc">
                            <table id="tblpeople-stats">
                                <thead>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    {
                                        this.state.companies.map((item, index) => {
                                            return (
                                                <tr key={index} className="cname" data-c="1">
                                                    <td style={{ width: 170, textAlign: 'left' }} onMouseMove={() => this.mouseMove(index)} onClick={() => this.onClickCompany(item)}>
                                                        <p className="btncmp" >{item.name} </p>
                                                    </td>
                                                    <td>
                                                        {item.score != undefined ? <span class="highlight-badge stats">{item.score}%</span> : null}
                                                    </td>
                                                    <td>
                                                        {this.state.loginUser.userType.includes(userType.superAdmin) ? <span title="Delete" id={"cd" + (index + 1)} className={this.state.companyHovered == index ? "btn-cdelete" : "btn-cdelete collapse"} onClick={() => this.deleteCompany(item)}>X</span> : null}
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <Modal isOpen={this.state.isAddCompany}>
                    <ModalHeader>
                        <h3>Add Company</h3>
                    </ModalHeader>
                    <ModalBody>
                        <input id="companyName" type="text" title="Enter company name" placeholder="company name" className="form-control" value={this.state.companyName} onChange={(e) => this.handleChange(e)}></input>
                    </ModalBody>
                    <ModalFooter>
                        <button
                            type="button"
                            className="btn btn-secondary"
                            onClick={this.toggle}>
                            Close
                        </button>
                        &nbsp;
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => this.save()}>
                            Save
                        </button>
                    </ModalFooter>
                </Modal>
            </section>
            </>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Company)