import React, { Component } from 'react';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as companyActions from '../../redux/actions/companyActions';

const actions = [
    companyActions
]

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshCount: props.refreshCount,
            activeCompany: props.activeCompany,
            companyScore: props.company.companyScore
        }
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.refreshCount != props.refreshCount && this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany, refreshCount: props.refreshCount }, () => {
                this.setData();
            });
            //Load data
        }
        if (this.state.activeCompany != props.activeCompany) {
            this.setState({ activeCompany: props.activeCompany });
        }
        if (this.state.companyScore != props.company.companyScore) {
            this.setState({ companyScore: props.company.companyScore });
        }
    }
    setData() {
        this.props.actions.updateCompanyScore(this.state.activeCompany._id, this.state.activeCompany.score);
        this.props.actions.updateEnergyScore(this.state.activeCompany.energyScore);
        this.props.actions.updateTrustScore(this.state.activeCompany.trustScore);
        this.props.actions.updateSeatScore(this.state.activeCompany.seatScore);
    }

    render() {
        return (
            <section id="cmpmenu" className="collapse">
                <div className="menu">
                    <button className="img-btn" type="button" style={{ paddingLeft: 10 }} onClick={() => this.props.nav("cmpmenu", "company")}><img src="img/back-arw.png" /></button>
                    <button className="lik-btn" type="button" style={{ marginTop: 20 }} onClick={() => this.props.nav("cmpmenu", "warroom")}>War Room</button>
                    {/* {
                        this.props.company.companies.map((item, index) => {
                            return (<button key={index} className={"lik-btn" + (this.state.activeCompany != undefined && item._id == this.state.activeCompany._id ? " active" : "")} type="button" onClick={() => this.props.nav("cmpmenu", "cmpmenu", { type: "company", value: item })}>{item.name}</button>)
                        })
                    } */}
                    {/* <button className="lik-btn" type="button">Company A</button>
                    <button className="lik-btn active" type="button" onClick={() => this.props.nav("cmpmenu", "people")}>Company B</button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("cmpmenu", "people")}>Company C</button> */}
                </div >
                <div className="main-menu">
                    <p className="h1 center">{this.state.activeCompany != undefined ? this.state.activeCompany.name : ""}<span className="highlight-badge">{this.state.companyScore}%</span></p>
                    <table>
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td><button id="_btnpeople" className="menu-item float-left"
                                    onClick={() => this.props.nav("cmpmenu", "people", { type: "company", value: this.state.activeCompany })}>People</button></td>
                                <td></td>
                                <td></td>
                                <td><button className="menu-item float-left" onClick={() => this.props.nav("cmpmenu", "numbers")}>Numbers</button></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><button className="menu-item b float-left" onClick={() => this.props.nav("cmpmenu", "strategy")}>Strategy</button></td>
                                <td><button className="menu-item b float-right" onClick={() => this.props.nav("cmpmenu", "execution")}>Execution</button>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </section>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
