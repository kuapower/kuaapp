import React, { Component } from 'react';
import { apiURL } from '../../common/global';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as userActions from '../../redux/actions/userActions';
import { userType } from '../../common/enum';
//import { userType } from '../../api/models/common/enum';

const actions = [
    userActions
]

class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginUser: props.user.user,
            users: [],
            selectedUser: undefined,
            companies: props.company.companies,
            name: '',
            email: '',
            selectedCompany: undefined,
            selectedUserType: undefined,
            refreshCount: props.refreshCount,
        }
    }
    componentDidMount() {
        this.getAllUser();
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.companies != props.company.companies) {
            this.setState({ companies: props.company.companies });
        }
        if (this.state.loginUser != props.user.user) {
            this.setState({ loginUser: props.user.user }, () => {
                this.getAllUser();
            });
        }
    }
    async getAllUser() {
        let url = apiURL + "user";
        if (!this.state.loginUser.userType.includes(userType.superAdmin)) {
            url = url + "?userId=" + this.state.loginUser._id;
        }
        const res = await fetch(url);
        const json = await res.json();
        if (json.success) {
            this.setState({ users: json.data });
        }
        else {
            alert("Error while getting users");
        }
    }
    saveUser() {
        if (this.state.selectedCompany != undefined && this.state.selectedCompany != "0"
            && this.state.selectedUserType != undefined && this.state.selectedUserType != "0" &&
            this.state.email.trim() != '' && this.state.email.trim() != undefined && this.state.email.trim() != null && this.validateEmail(this.state.email) &&
            this.state.name.trim() != '' && this.state.name.trim() != undefined && this.state.name.trim() != null) {
            //Go to Save
            let userToSave = {
                name: this.state.name,
                email: this.state.email,
                companyId: this.state.selectedCompany,
                userType: Number(this.state.selectedUserType)
            };
            if (this.state.selectedUser != undefined) {
                userToSave._id = this.state.selectedUser._id;
            }
            fetch(apiURL + 'user', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(userToSave),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        this.clearUser();
                        this.getAllUser();
                    }
                    else {
                        alert(rsp.message);
                    }
                })
                .catch((error) => {
                    alert("Error" + error);
                });
        }
        else {
            alert("Enter proper user data");
        }
    }
    clearUser() {
        this.setState({
            selectedUser: undefined,
            name: '',
            email: '',
            selectedCompany: undefined,
            selectedUserType: 0
        });
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    onchangeSelectCompany(e) {
        this.setState({ selectedCompany: e.target.value });
    }
    onchangeSelectUserType(e) {
        this.setState({ selectedUserType: e.target.value });
    }
    deleteUser(item) {
        if (confirm("Are you sure want to delete?")) {
            fetch(apiURL + 'user', {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: item._id,
                }),
            })
                .then((response) => response.json())
                .then((rsp) => {
                    if (rsp.success) {
                        this.clearUser();
                        this.getAllUser();
                    }
                    else {
                        alert(rsp.message);
                    }
                })
                .catch((error) => {
                    alert("Error" + error);
                });
        }
    }
    onchangeSelectUser(user) {
        this.setState({ selectedUser: user, name: user.name, email: user.email, selectedCompany: user.companyId, selectedUserType: user.userType });
    }
    render() {
        return (
            <section id="user" className="collapse" >
                <div className="menu">
                    <button className="img-btn" type="button" onClick={() => this.props.nav("user", "company")}><img src="img/back-arw.png" /></button>
                </div>
                <div className="user-action">
                    {this.state.loginUser.userType.includes(userType.cAdmin) || this.state.loginUser.userType.includes(userType.superAdmin) ?
                        <div className="adduser-section">
                            <div className="form-group">
                                <input className="form-control" type="text" placeholder="Name" value={this.state.name} onChange={(e) => { this.setState({ name: e.target.value }) }} />
                            </div>
                            <div className="form-group">
                                <input className="form-control" type="text" placeholder="Email" value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }) }} />
                            </div>
                            {/* <div className="form-group">
                            <input className="form-control" type="text" placeholder="password" />
                        </div> */}
                            <div className="form-group">
                                <select id="selCompany" className="form-control" onChange={(e) => this.onchangeSelectCompany(e)} value={this.state.selectedCompany != undefined && this.state.selectedCompany._id}>
                                    <option value="0">Select Company </option>
                                    {
                                        this.state.companies.map((item, index) => {
                                            //if (this.state.selectedCompany != undefined && item._id == this.state.selectedCompany._id)
                                            return (<option key={index} value={item._id}>{item.name}</option>)
                                            // else
                                            //     return (<option key={index} value={item._id} >{item.name}</option>)
                                        })
                                    }
                                </select>
                            </div>
                            <div className="form-group">
                                <select className="form-control" onChange={(e) => this.onchangeSelectUserType(e)} value={this.state.selectedUserType}>
                                    <option value="0">Select User type </option>
                                    <option value="2" >Company Admin</option>
                                    <option value="3" >Team member</option>
                                </select>
                            </div>
                            <div className="form-group">
                                <input className="form-control" type="button" value="Save" onClick={() => this.saveUser()} />
                                <input className="form-control" style={{ marginLeft: 5 }} type="button" value="Cancel" onClick={() => this.clearUser()} />
                            </div>
                        </div>
                        : null}
                    <table id="tblenergy" className="center">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>User type</th>
                                {this.state.loginUser.userType.includes(userType.cAdmin) || this.state.loginUser.userType.includes(userType.superAdmin) ?
                                    <th>Action</th> : null}
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.users.map((item, index) => {
                                    let userTypes = [];
                                    if (typeof (item.userType) == Array)
                                        item.userType.forEach(usertype => {
                                            userTypes.push(usertype == 2 ? "Company Admin" : "Team Member");
                                        });
                                    else
                                        userTypes.push(item.userType == 2 ? "Company Admin" : "Team Member");
                                    return (
                                        <tr key={index} className="u_4 user-item" onClick={() => this.onchangeSelectUser(item)}>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>
                                                {item.companyId.map(e => e.name).join(",")}
                                            </td>
                                            <td>
                                                {userTypes.join(",")}
                                            </td>
                                            {this.state.loginUser.userType.includes(userType.cAdmin) || this.state.loginUser.userType.includes(userType.superAdmin) ?
                                                <td style={{ textAlign: 'center', cursor: 'pointer' }} title={"Click to delete"} onClick={() => this.deleteUser(item)}><span>X</span></td> : null}
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>

                </div>
            </section>
        );
    }
}


//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)