import React, { Component } from 'react';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as companyActions from '../../redux/actions/companyActions';

const actions = [
    companyActions
]

class Warroom extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <section id="warroom" className="collapse">
                <div className="menu">
                    <button className="img-btn" type="button" onClick={() => this.props.nav("warroom", "cmpmenu")}><img src="img/back-arw.png" /></button>
                    {/* {
                    this.props.company.companies.map((item, index) => {
                        return (<button key={index} className={"lik-btn" + (this.state.activeCompany != undefined && item._id == this.state.activeCompany._id ? " active" : "")} type="button" onClick={() => this.props.nav("cmpmenu", "cmpmenu", { type: "company", value: item })}>{item.name}</button>)
                    })
                } */}
                    {/* <button className="lik-btn" type="button">Company A</button>
                <button className="lik-btn active" type="button" onClick={() => this.props.nav("cmpmenu", "people")}>Company B</button>
                <button className="lik-btn" type="button" onClick={() => this.props.nav("cmpmenu", "people")}>Company C</button> */}
                </div >
                <div className="main-menu">
                    <p className="h1 center">War Room (coming soon)</p>
                    <p className="h2 center">You will be able to upload picture here.</p>
                </div>
            </section>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Warroom)

