import React, { Component } from 'react';
import { apiURL } from '../../common/global';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';

const actions = [

];

class Numbers extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
    }
    render() {
        return (
            <section id="numbers" className="collapse">
                <div className="menu">
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("numbers", "people")}>People</button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("numbers", "strategy")}>Strategy</button>
                    <button className="lik-btn " type="button" onClick={() => this.props.nav("numbers", "execution")}>Execution </button>
                    <button className="lik-btn active" type="button">Numbers</button>
                </div>
                <div id="number-data">
                    <table id="tblnumber" className="center">
                        <thead>
                            <tr>
                                <th className="no-bg no-border" colSpan="3">
                                    <button id="selectweeks" className="lik-btn active" type="button">Current 10 weeks</button>
                                </th>
                                <th className="no-bg no-border"></th>
                                <th className="no-bg no-border"></th>
                                <th className="no-bg no-border"></th>
                                <th className="no-bg no-border"></th>
                                <th className="no-bg no-border"></th>
                                <th className="no-border no-bg">
                                    History
                            </th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>Who</th>
                                <th>Measurable</th>
                                <th className="text-right">Goal</th>
                                <th>1</th>
                                <th>2</th>
                                <th>3</th>
                                <th>4</th>
                                <th>5</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Adam</td>
                                <td><input className="" type="text" /></td>
                                <td><input className="" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Jeff</td>
                                <td><input className="" type="text" /></td>
                                <td><input className="" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Mark</td>
                                <td><input className="" type="text" /></td>
                                <td><input className="" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Peter</td>
                                <td><input className="" type="text" /></td>
                                <td><input className="" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>James</td>
                                <td><input className="" type="text" /></td>
                                <td><input className="" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Heli</td>
                                <td><input className="" type="text" /></td>
                                <td><input className="" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                                <td><input className="weekv" type="text" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </section>
        )
    }
}


//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Numbers)
