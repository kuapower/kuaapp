import React, { Component } from 'react';
import Company from './Company';
import Menu from './Company/Menu';
import Users from './Company/Users';
import Warroom from './Company/Warroom';
import Execution from './Execution';
import Numbers from './Numbers';
import People from './People';
import Energy from './People/Energy';
import Seats from './People/Seats';
import Trust from './People/Trust';
import Strategy from './Strategy';
class KuaPower extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //refreshCount: 0,
            //screenName: undefined
            activeCompany: undefined
        }
    }
    nav = (from, to, param) => {
        $('#' + from).fadeOut(500, function () {
            $('#' + to).fadeIn(500);
        });
        if (param != undefined && param.type == "company") {
            this.setState({ activeCompany: param.value, refreshCount: ++this.state.refreshCount })
        }
        //this.setState({ refreshCount: ++this.state.refreshCount, screenName: to });
    }
    render() {
        return (
            <div>
                <div id="top-alert" class="alert alert-danger center collapse" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>No users under this company. Please add it in team.</strong>
                </div>
                <Company nav={this.nav} {...this.state} />
                <Users nav={this.nav} {...this.state} />
                <Menu nav={this.nav} {...this.state} />
                <Warroom nav={this.nav} {...this.state} />
                <People nav={this.nav} {...this.state} />
                <Seats nav={this.nav} {...this.state} />
                <Energy nav={this.nav} {...this.state} />
                <Trust nav={this.nav} {...this.state} />
                <Numbers nav={this.nav} {...this.state} />
                <Strategy nav={this.nav} {...this.state} />
                <Execution nav={this.nav} {...this.state} />
            </div>
        )
    }
}

export default KuaPower