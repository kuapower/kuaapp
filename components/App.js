import React, { Component } from 'react';
import Head from 'next/head';
import Login from './Login';
import KuaPower from './KuaPower';

//Binding
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import * as userActions from '../redux/actions/userActions';

const actions = [
    userActions
]

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: props.user.isLoggedIn,
        };
    }
    UNSAFE_componentWillReceiveProps(props) {
        if (this.state.isLoggedIn != props.user.isLoggedIn) {
            this.setState({ isLoggedIn: props.user.isLoggedIn });
        }
    }
    render() {
        return (
            <div>
                <Head>
                    <title>Kua Power</title>
                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                    <link href="https://fonts.googleapis.com/css2?family=Annie+Use+Your+Telescope&display=swap" rel="stylesheet" />
                    <link href="/css/bootstrap.css" rel="stylesheet" />
                    <link href="/css/style.css" rel="stylesheet" />
                    <link href="/css/jquery.orgchart.css" rel="stylesheet" />
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
                    <script src="/js/jquery-1.11.1.min.js" />
                    <script src="/js/browerback.js" />
                    <script src="/js/bootstrap.min.js" />
                    <script src="/js/jquery.orgchart.js" />
                    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                </Head>
                {!this.state.isLoggedIn ?
                    <Login /> : <KuaPower />}
            </div>
        )
    }
}

//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)