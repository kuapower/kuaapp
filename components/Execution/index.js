import React, { Component } from 'react';
import { apiURL } from '../../common/global';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';

const actions = [

];

class Execution extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
        const colorArray = ['#fff', '#92D050', '#FFFF02', '#FF0000'];
        $("#tblexecution tbody td").dblclick(function () {

            var color = $(this).data('color') == undefined ? 0 : $(this).data('color') * 1;

            if (color == undefined || color == (colorArray.length - 1)) {
                $(this).css('background-color', colorArray[0]);
                $(this).data('color', '0');
            }
            else {
                $(this).css('background-color', colorArray[color + 1]);
                $(this).data('color', color + 1);
            }
        });

    }
    render() {
        return (
            <section id="execution" className="collapse">
                <div className="menu">
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("execution", "people")}>People</button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("execution", "strategy")}>Strategy</button>
                    <button className="lik-btn active" type="button">Execution </button>
                    <button className="lik-btn" type="button" onClick={() => this.props.nav("execution", "numbers")}> Numbers</button >
                </div>
                <div>
                    <div class="container center">
                        <a id="btnissue" class="lik-btn active" type="button" href="#popup1">Issues list</a>
                        <table id="tblexecution" class="center">
                            <thead>
                                <tr>
                                    <th class="no-bg no-border" colSpan="2">
                                        <button id="selectweeks" class="lik-btn active" type="button">Current 10 weeks</button>
                                    </th>
                                    <th class="no-bg no-border"></th>
                                    <th class="no-bg no-border"></th>
                                    <th class="no-bg no-border"></th>
                                    <th class="no-bg no-border"></th>
                                    <th class="no-border no-bg">History</th>
                                </tr>
                                <tr>
                                    <th>Differentiators</th>
                                    <th>Week 1</th>
                                    <th>Week 2</th>
                                    <th>Week 3</th>
                                    <th>Week 4</th>
                                    <th>Week 5</th>
                                    <th style={{ minWidth: 190 }}>Completion %</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td id="makesusdifans1"></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" value="80%" /></td>
                                </tr>
                                <tr>

                                    <td id="makesusdifans2"></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                </tr>
                                <tr>

                                    <td id="makesusdifans3"></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                    <td><input class="editweek " type="text" /></td>
                                </tr>

                            </tbody>

                        </table>
                    </div>
                </div>
                <div id="popup1" class="overlay">
                    <div class="popup">

                        <a class="close" href="#">Close</a>
                        <div class="content">

                            <div class="issuelist">
                                <div class="iname" data-i="1">
                                    <p class="btniue">issue A </p><span title="Delete" id="id1"
                                        class="btn-idelete collapse">X</span>
                                </div>
                                <div class="iname" data-i="2">
                                    <p class="btniue">issue B </p><span title="Delete" id="id2"
                                        class="btn-idelete collapse">X</span>
                                </div>
                                <div class="iname" data-i="3">
                                    <p class="btniue">issue C </p><span title="Delete" id="id3"
                                        class="btn-idelete collapse">X</span>
                                </div>
                            </div>
                            <input id="addissue" type="text" placeholder="Enter new issue" />

                        </div>
                    </div>
                </div>

            </section>
        )
    }
}


//Binding
function mapStateToProps(state) {
    return { ...state }
}
function mapDispatchToProps(dispatch) {
    const creators = Map().merge(...actions)
        .filter(value => typeof value === 'function')
        .toObject()
    return { actions: bindActionCreators(creators, dispatch), dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Execution)
