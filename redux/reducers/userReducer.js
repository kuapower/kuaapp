import * as types from '../constants/userActionTypes';

const initialState = {
    user: null,
    isLoggedIn: false,
    loginError: undefined
};

export default function user(state = initialState, action = {}) {
    switch (action.type) {
        case types.LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                user: action.user,
                loginError: undefined
            };
        case types.LOGIN_ERROR:
            return {
                ...state,
                isLoggedIn: false,
                loginError: { errcode: action.errcode, errmsg: action.errmsg }
            };
        case types.LOGIN_CLEAR:
            return {
                ...state,
                isLoggedIn: false,
                loginError: undefined
            };
        case types.LOGOUT_SUCCESS:
            return {
                ...state,
                user: null,
                isLoggedIn: false,
                loginError: undefined
            };
        default:
            return state;
    }
}

