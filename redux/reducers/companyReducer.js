import * as types from '../constants/companyActionTypes';

const initialState = {
    isCompanyGet: false,
    companies: [],
    companyError: undefined,
    isEnergyScoreUpdated: false,
    energyScore: 0,
    isTrustScoreUpdated: false,
    trustScore: 0,
    isSeatScoreUpdated: false,
    seatScore: 0,
    companyScore: 0,
    isCompanyScoreUpated: false,
    errorCompanyScore: undefined
};

export default function company(state = initialState, action = {}) {
    switch (action.type) {
        case types.GET_COMPANY_SUCCESS:
            return {
                ...state,
                isCompanyGet: true,
                companies: action.companies,
                companyError: undefined
            };
        case types.GET_COMPANY_ERROR:
            return {
                ...state,
                isCompanyGet: false,
                companies: [],
                companyError: { errcode: action.errcode, errmsg: action.errmsg }
            };
        case types.UPDATE_ENERGY_SCORE:
            return {
                ...state,
                isEnergyScoreUpdated: true,
                energyScore: action.energyScore,
                //companyScore: Math.round((state.trustScore + state.seatScore + action.energyScore) / 3)
            };
        case types.CLEAR_ENERGY_SCORE:
            return {
                ...state,
                isEnergyScoreUpdated: false,
            };
        case types.UPDATE_TRUST_SCORE:
            return {
                ...state,
                isTrustScoreUpdated: true,
                trustScore: action.trustScore,
                //companyScore: Math.round((action.trustScore + state.seatScore + state.energyScore) / 3)
            };
        case types.CLEAR_TRUST_SCORE:
            return {
                ...state,
                isTrustScoreUpdated: false,
            };
        case types.UPDATE_SEAT_SCORE:
            return {
                ...state,
                isSeatScoreUpdated: true,
                seatScore: action.seatScore,
                //companyScore: Math.round((state.trustScore + action.seatScore + state.energyScore) / 3)
            };
        case types.CLEAR_SEAT_SCORE:
            return {
                ...state,
                isSeatScoreUpdated: false,
            };
        case types.UPDATE_COMPANY_SCORE:
            return {
                ...state,
                isCompanyScoreUpated: true,
                companyScore: Number(action.score),
                errorCompanyScore: undefined
            };
        case types.UPDATE_COMPANY_SCORE_ERROR:
            return {
                ...state,
                isCompanyScoreUpated: false,
                companyScore: 0,
                errorCompanyScore: { errcode: action.errcode, errmsg: action.errmsg }
            };
        case types.CLEAR_COMPANY_SCORE:
            return {
                ...state,
                isCompanyScoreUpated: false,
                //companyScore: 0,
                errorCompanyScore: undefined
            };
        default:
            return state;
    }
}