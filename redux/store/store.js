import { createStore, applyMiddleware } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import thunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import rootReducer from '../reducers';

const persistConfig = {
    key: 'root',
    storage,
}
const persistedReducer = persistReducer(persistConfig, rootReducer)
export default function configureStore(initialState = {}) {
    const store = createStore(
        persistedReducer,
        initialState,
        applyMiddleware(thunk)
    );
    const pstore = persistStore(store); //store with localdb
    return store;
}