import { userType } from '../../common/enum';
import { apiURL, httpMessage } from '../../common/global';
import {
    GET_COMPANY_SUCCESS, GET_COMPANY_ERROR, UPDATE_ENERGY_SCORE, CLEAR_ENERGY_SCORE, UPDATE_TRUST_SCORE, CLEAR_TRUST_SCORE,
    UPDATE_SEAT_SCORE, CLEAR_SEAT_SCORE, UPDATE_COMPANY_SCORE, CLEAR_COMPANY_SCORE, UPDATE_COMPANY_SCORE_ERROR
} from '../constants/companyActionTypes';

//Login
export function getCompanySuccess(companies) {
    return {
        type: GET_COMPANY_SUCCESS,
        companies,
    }
}
export function getCompanyError(errcode, errmsg) {
    return {
        type: GET_COMPANY_ERROR,
        errcode,
        errmsg
    }
}

export function getCompany(user) {
    let url = apiURL + "company";
    if (user.userType != userType.superAdmin) {
        url = url + "?userId=" + user._id;
    }
    return dispatch =>
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success)
                    dispatch(getCompanySuccess(rsp.data));
                else
                    dispatch(getCompanyError(-1, ""));
            })
            .catch((error) => {
                dispatch(getCompanyError(-200, httpMessage.networkError));
            });
}

export function updateEnergyScore(energyScore) {
    return {
        type: UPDATE_ENERGY_SCORE,
        energyScore,
    }
}
export function clearEnergyScore() {
    return {
        type: CLEAR_ENERGY_SCORE
    }
}
export function updateTrustScore(trustScore) {
    return {
        type: UPDATE_TRUST_SCORE,
        trustScore,
    }
}
export function clearTrustScore() {
    return {
        type: CLEAR_TRUST_SCORE
    }
}

export function updateSeatScore(seatScore) {
    return {
        type: UPDATE_SEAT_SCORE,
        seatScore,
    }
}
export function clearSeatScore() {
    return {
        type: CLEAR_SEAT_SCORE
    }
}

export function updateCompanyScoreSuccess(score) {
    return {
        type: UPDATE_COMPANY_SCORE,
        score,
    }
}
export function updateCompanyScoreError(errcode, errmsg) {
    return {
        type: UPDATE_COMPANY_SCORE_ERROR,
        errcode,
        errmsg
    }
}
export function clearCompanyScore() {
    return {
        type: CLEAR_COMPANY_SCORE
    }
}

export function updateCompanyScore(companyId, score) {
    return dispatch =>
        fetch(apiURL + 'companyScore', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                _id: companyId,
                score: score,
            }),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success)
                    dispatch(updateCompanyScoreSuccess(rsp.data));
                else
                    dispatch(updateCompanyScoreError(rsp.code, rsp.message));
            })
            .catch((error) => {
                console.log(error);
                dispatch(updateCompanyScoreError(-200, httpMessage.networkError));
            });


}