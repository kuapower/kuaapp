import { apiURL, httpMessage } from '../../common/global';
import {
    LOGIN_SUCCESS, LOGIN_ERROR, LOGIN_CLEAR, LOGOUT_SUCCESS
} from '../constants/userActionTypes';

//Login
export function userLoginSuccess(user) {
    return {
        type: LOGIN_SUCCESS,
        user,
    }
}
export function userLoginError(errcode, errmsg) {
    return {
        type: LOGIN_ERROR,
        errcode,
        errmsg
    }
}
export function userLoginClear() {
    return {
        type: LOGIN_CLEAR,
    }
}

export function logout() {
    return {
        type: LOGOUT_SUCCESS,
    }
}

export function login(email, password) {
    return dispatch =>
        fetch(apiURL + 'auth', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
            .then((response) => response.json())
            .then((rsp) => {
                if (rsp.success)
                    dispatch(userLoginSuccess(rsp.user));
                else
                    dispatch(userLoginError(rsp.code, rsp.message));
            })
            .catch((error) => {
                dispatch(userLoginError(-200, httpMessage.networkError));
            });


}