const config = require("../config");
var nodemailer = require('nodemailer');

async function sendEmail(toEmail, emailSubject, emailContent) {
    if (emailContent != "") {
        var smtpTransport = nodemailer.createTransport({
            host: config.SmtpServer,
            port: config.SmtpPort,
            ignoreTLS: true,
            secure: false,
            secureConnection: false,
            requiresAuth: true,
            auth: {
                user: config.FromEmail,
                pass: config.EmailPassword
            }
        });

        var mailOptions = {
            from: "Kua App <" + config.FromEmail + ">",
            to: toEmail,
            replyTo: config.FromEmail,
            subject: emailSubject,
            html: emailContent
        };
        try {
            let info = await smtpTransport.sendMail(mailOptions);
            return 1;
        } catch (error) {
            if (error) {
                console.log(error);
                return -1;
            }
        }
    }
    else
        return 0;
}

async function sendAdminEmail(fromEmail, toEmail, emailSubject, emailContent) {
    if (emailContent != "") {
        var smtpTransport = nodemailer.createTransport({
            host: config.SmtpServer,
            port: config.SmtpPort,
            ignoreTLS: true,
            secure: false,
            secureConnection: false,
            requiresAuth: true,
            auth: {
                user: config.FromEmail,
                pass: config.EmailPassword
            }
        });

        var mailOptions = {
            from: fromEmail,
            to: toEmail,
            replyTo: fromEmail,
            subject: emailSubject,
            html: emailContent
        };
        try {
            let info = await smtpTransport.sendMail(mailOptions);
            return 1;
        } catch (error) {
            if (error) {
                console.log(error);
                return -1;
            }
        }
    }
    else
        return 0;
}

module.exports = { sendEmail, sendAdminEmail };