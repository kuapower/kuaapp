exports.userType = {
    superAdmin: 1,
    cAdmin: 2,
    teamMember: 3
};

exports.activityType = {
    energizing: 1,
    ok: 2,
    draining: 3
};

exports.sgrade = {
    A: 1,
    B: 2,
    C: 3,
    tA: 4
};