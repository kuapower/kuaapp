//Api URL
//export const apiURL = 'http://localhost:3000/api/';
//export const apiURL = 'https://kuapower.herokuapp.com/api/';
//Live
export const apiURL = '/api/';
//Messages
export const httpMessage = {
    networkError: 'Network error.'
}

export const generatePassword = () => {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}