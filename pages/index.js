import React from 'react'
import { Provider } from 'react-redux'
import configureStore from '../redux/store/store';
import App from '../components/App';
const Home = () => (
  <Provider store={configureStore()}>
    <App />
  </Provider >
)
export default Home