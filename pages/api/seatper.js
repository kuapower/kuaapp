import createHandler from '../../api/middleware/createhandler';
import SeatPer from '../../api/models/SeatPerModel';
const handler = createHandler();

//Create Seat Per
handler.post(async (req, res) => {
    try {
        if (req.body._id != undefined) {
            let seatUpdate = await SeatPer.updateOne({ _id: req.body._id }, req.body);
            res.status(200).send({ success: true, data: seatUpdate });
        }
        else {
            var newSeat = new SeatPer(req.body);
            var createSeat = await newSeat.save();
            res.status(200).send({ success: true, data: createSeat });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All Seat Per
handler.get(async (req, res) => {
    const {
        query: { seatId },
    } = req
    try {
        let seatPers = await SeatPer.find({ seatId: seatId, isActive: true }).populate('userId');
        res.status(200).send({ success: true, data: seatPers });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Delete SeatPer
handler.delete(async (req, res) => {
    try {
        await SeatPer.deleteOne({ _id: req.body.id });
        res.status(200).send({ success: true });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
})

export default (req, res) => handler.run(req, res) 