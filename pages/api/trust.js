import createHandler from '../../api/middleware/createhandler';
import Trusts from '../../api/models/TrustModel';
import Users from '../../api/models/UserModel';
import { ObjectId } from 'mongodb';
const handler = createHandler();

//Create Trust
handler.post(async (req, res) => {
    try {
        if (req.body.sessionDateTime != undefined && req.body.trustData != undefined) { 
            let data = req.body.trustData;
            for (let i = 0; i < data.length; i++) {
                const tRow = data[i];
                for (let j = 0; j < tRow.data.length; j++) {
                    const tTemp = tRow.data[j];
                    let t = {
                        userId: tTemp.userId._id,
                        sessionDateTime: req.body.sessionDateTime,
                        type: tTemp.type,
                        tValue: tTemp.tValue,
                        trustByUserId: tTemp.trustByUserId,
                        companyId: req.body.companyId,
                    }
                    var newTrust = new Trusts(t);
                    var createTrust = await newTrust.save();
                }
            }
            
            res.status(200).send({ success: true });
                //     var createTrust = await newTrust.save();
        }
        // if (req.body._id != undefined) {
        //     let trustUpdate = await Trusts.updateOne({ _id: req.body._id }, req.body);
        //     res.status(200).send({ success: true, data: trustUpdate });
        // }
        // else {
        //     var newTrust = new Trusts(req.body);
        //     var createTrust = await newTrust.save();
        //     res.status(200).send({ success: true, data: createTrust });
        // }
        // res.status(200).send({ success: true });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All Trust
handler.get(async (req, res) => {
    const {
        query: { companyId, trustByUserId, isPast, currentSessionTime },
    } = req
    try {
        let trusts = [];
        let users = await Users.find({ companyId: companyId });
        var userArray = users.map(function (item) {
            return item['_id'];
        });
        if (isPast == 'undefined' && currentSessionTime == 'undefined') {
            trusts = await Trusts.find({ 'userId': { $in: userArray }, 'trustByUserId': ObjectId(trustByUserId), isActive: true }).populate('userId');
            let sessionDateTime = trusts[trusts.length - 1].sessionDateTime;
            if (sessionDateTime != undefined) {
                trusts = trusts.filter(a => a.sessionDateTime == sessionDateTime);
            }
        }
        else if (isPast && currentSessionTime != undefined) {
            trusts = await Trusts.find({ 'userId': { $in: userArray }, isActive: true }).populate('userId');
            trusts = trusts.filter(a => Date(a.sessionDateTime) < Date(currentSessionTime));
        }
        res.status(200).send({ success: true, data: trusts });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});
// Delete the trust for same company
handler.delete(async (req, res) => {
    try {
        if (req.body.isActive) {
            await Trusts.deleteMany({ companyId: req.body._id });
            res.status(200).send({ success: true});
        }
        else {
            alert("not here");
            res.status(200).send({ success: false, code: -1, message: 'Something went wrong! Please try again.' });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res)