import { ObjectId } from 'mongodb';
import createHandler from '../../api/middleware/createhandler';
import Companies from '../../api/models/CompanyModel';
import Users from '../../api/models/UserModel';
import Seats from '../../api/models/SeatModel';
import Trusts from '../../api/models/TrustModel';
import Energy from '../../api/models/EnergyModel';
import SeatPer from '../../api/models/SeatPerModel';
const handler = createHandler();

//Create Company
handler.post(async (req, res) => {
    try {
        var newCompany = new Companies(req.body);
        var createCompany = await newCompany.save();
        res.status(200).send({ success: true, data: createCompany });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All Company
handler.get(async (req, res) => {
    const {
        query: { userId },
    } = req
    try {
        let companies = [];
        if (userId != undefined) {
            let user = await Users.findOne({ _id: ObjectId(userId) }, { 'pwd': 0 }).lean();
            companies = await Companies.find({ '_id': user.companyId }, {}, { sort: { score: -1 } });
        }
        else {
            companies = await Companies.find({ isActive: true }, {}, { sort: { score: -1 } });
        }
        res.status(200).send({ success: true, data: companies });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Delete Company
handler.delete(async (req, res) => {
    try {
        let users = await Users.find({ companyId: req.body.id }).lean();
        for (let i = 0; i < users.length; i++) {
            const user = users[i];
            await Trusts.deleteMany({ userId: user._id });
            await Energy.deleteMany({ userId: user._id });
            await SeatPer.deleteMany({ userId: user._id });
        }
        await Seats.deleteMany({ companyId: req.body.id });
        await Users.deleteMany({ companyId: req.body.id });
        await Companies.deleteOne({ _id: req.body.id });
        res.status(200).send({ success: true });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
})

export default (req, res) => handler.run(req, res) 