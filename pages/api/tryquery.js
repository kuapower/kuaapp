import createHandler from '../../api/middleware/createhandler';
const handler = createHandler();

import { sendAdminEmail } from '../../common/emailSend';
import config from '../../config';

//Post method
handler.post(async (req, res) => {
    try {
        if (req.body.email != undefined) {
            var emailSubject = "I want to join and try KUA Power";
            var emailContent = "<div><table><tr>" +
                "<td style='padding: 20px; font-size: 16px; line-height: 20px;'>" +
                "<p>Hello,</p>" +
                "<p>I'm interested in KUA Power</p>" +
                "</td>" +
                "</tr></table></div>";
            let emailSent = await sendAdminEmail(req.body.email, config.contactemail, emailSubject, emailContent);
            if (emailSent == 1) {
                res.status(200).send({ success: true });
            }
            else {
                res.status(200).send({ success: false, code: -1 });
            }
        }
        else {
            res.status(200).send({ success: false, code: -1 });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, });
    }
});

handler.put(async (req, res) => {
    try {
        if (req.body.email != undefined && req.body.query != undefined) {
            var emailSubject = "I have a query";
            var emailContent = "<div><table><tr>" +
                "<td style='padding: 20px; font-size: 16px; line-height: 20px;'>" +
                "<p>Hello,</p>" +
                "<p>" +
                "##query##" +
                "</p>" +
                "</div>" +
                "</td>" +
                "</tr></table></div>";
            emailContent = emailContent.replace("##query##", req.body.query);
            let emailSent = await sendAdminEmail(req.body.email, config.contactemail, emailSubject, emailContent);
            if (emailSent == 1) {
                res.status(200).send({ success: true });
            }
            else {
                res.status(200).send({ success: false, code: -1 });
            }
        }
        else {
            res.status(200).send({ success: false, code: -1 });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, });
    }
});

export default (req, res) => handler.run(req, res) 