import createHandler from '../../api/middleware/createhandler';
import { sgrade } from '../../common/enum';
import Seats from '../../api/models/SeatModel';
import SeatPers from '../../api/models/SeatPerModel';
const handler = createHandler();

//Create Seat
handler.post(async (req, res) => {
    try {
        if (req.body._id != undefined) {
            let seatUpdate = await Seats.updateOne({ _id: req.body._id }, req.body);
            res.status(200).send({ success: true, data: seatUpdate });
        }
        else {
            var newSeat = new Seats(req.body);
            var createSeat = await newSeat.save();
            res.status(200).send({ success: true, data: createSeat });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All Seat
handler.get(async (req, res) => {
    const {
        query: { companyId },
    } = req
    try {
        let seats = await Seats.find({ companyId: companyId, isActive: true }, {}, { sort: { parent: -1 } }).populate('userId').lean();
        if (seats.length == 0) {
            var newSeat = new Seats({
                companyId: companyId,
                position: 'Enter position',
                userId: undefined,
                description: 'Details here',
                parent: 0,
                parentSeatId: undefined
            });
            var createSeat = await newSeat.save();
            seats.push(createSeat);
        }
        for (let i = 0; i < seats.length; i++) {
            const seat = seats[i];
            let seatper = await SeatPers.find({ seatId: seat._id });
            if (seatper.length > 0) {
                let aGrade = seatper.filter(a => a.grade == sgrade.A).length;
                let agradePer = Math.round(aGrade / seatper.length * 100);
                seat.aper = Object.is(agradePer, NaN) ? 0 : agradePer;
            }
            else {
                let childSeats = seats.filter(a => a.parentSeatId == seat._id);
                let totalAper = 0;
                for (let j = 0; j < childSeats.length; j++) {
                    const cSeat = childSeats[j];
                    totalAper += cSeat.aper;
                }
                let aper = Math.round(totalAper / childSeats.length)
                seat.aper = aper != undefined && aper != null && !Object.is(aper, NaN) ? aper : 0;
            }
            seats[i] = seat;
        }
        seats.sort((a, b) => a.parent - b.parent);
        res.status(200).send({ success: true, data: seats });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Delete Seat
handler.delete(async (req, res) => {
    try {
        await Seats.deleteOne({ _id: req.body.id });
        res.status(200).send({ success: true });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
})

export default (req, res) => handler.run(req, res) 