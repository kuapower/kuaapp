import createHandler from '../../api/middleware/createhandler';
import Companies from '../../api/models/CompanyModel';
const handler = createHandler();

//Create Company
handler.post(async (req, res) => {
    try {
        if (req.body._id != undefined) {
            let companyUpdate = await Companies.updateOne({ _id: req.body._id }, req.body);
            res.status(200).send({ success: true, data: req.body.score });
        }
        else {
            res.status(200).send({ success: false, code: -1, message: 'Something went wrong! Please try again.' });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Save seat, trust, energy scores
handler.put(async (req, res) => {
    try {
        if (req.body.isSeat) {
            let companyUpdate = await Companies.updateOne({ _id: req.body._id }, { seatScore: req.body.seatScore });
            res.status(200).send({ success: true, data: req.body.seatScore });
        }
        else if (req.body.isTrust) {
            let companyUpdate = await Companies.updateOne({ _id: req.body._id }, { trustScore: req.body.trustScore });
            res.status(200).send({ success: true, data: req.body.trustScore });
        }
        else if (req.body.isEnergy) {
            let companyUpdate = await Companies.updateOne({ _id: req.body._id }, { energyScore: req.body.energyScore });
            res.status(200).send({ success: true, data: req.body.energyScore });
        }
        else {
            res.status(200).send({ success: false, code: -1, message: 'Something went wrong! Please try again.' });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res)