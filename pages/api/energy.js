import createHandler from '../../api/middleware/createhandler';
import Energy from '../../api/models/EnergyModel';
import Activity from '../../api/models/ActivityModel';
import Users from '../../api/models/UserModel';
import { ObjectId } from 'mongodb';
const handler = createHandler();

//Create Energy
handler.post(async (req, res) => {
    try {
        if (req.body.activityId == undefined) {
            var activity = new Activity({ name: req.body.activity, description: '' });
            var newActivity = await activity.save();
            req.body.activityId = newActivity._id;
        }
        let data = {
            _id: req.body._id,
            userId: req.body.userId,
            activityId: req.body.activityId,
            type: req.body.type,
            description: req.body.description,
        };
        if (data._id != undefined) {
            let energyUpdate = await Energy.updateOne({ _id: data._id }, data);
            res.status(200).send({ success: true, data: energyUpdate });
        }
        else {
            var newEnergy = new Energy(data);
            var createEnergy = await newEnergy.save();
            res.status(200).send({ success: true, data: createEnergy });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All Energy
handler.get(async (req, res) => {
    const {
        query: { companyId },
    } = req
    try {
        //Find users from company ID;
        var hex = /[0-9A-Fa-f]{6}/g;
        let id = (hex.test(companyId)) ? ObjectId(companyId) : companyId;
        let users = await Users.find({ companyId: companyId }).lean();
        var userArray = users.map(function (item) {
            return item['_id'] + "";
        });
        let energys = await Energy.find({ 'userId': { $in: userArray }, isActive: true }).populate('userId').populate('activityId');
        res.status(200).send({ success: true, data: energys });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Delete Energy
handler.delete(async (req, res) => {
    try {
        await Energy.deleteOne({ _id: req.body.id });
        res.status(200).send({ success: true });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
})


//Update energy
handler.put(async (req, res) => {
    try {
        if (req.body._id != undefined) {
            ("Seat to update", req.body);
            let energyUpdate = await Energy.updateOne({ _id: req.body._id }, req.body);
            res.status(200).send({ success: true, data: energyUpdate });
        }
        else {
            res.status(200).send({ success: false, code: -1, message: 'No energy exist.' });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res) 