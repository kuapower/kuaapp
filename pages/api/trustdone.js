import createHandler from '../../api/middleware/createhandler';
import TrustDone from '../../api/models/TrustDoneModel';
import { ObjectId } from 'mongodb';
const handler = createHandler();

//Create TrustDone
handler.post(async (req, res) => {
    try {
        var newTrustDone = new TrustDone(req.body);
        var created = await newTrustDone.save();
        res.status(200).send({ success: true, data: created });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All TrustDone
handler.get(async (req, res) => {
    const {
        query: { trustAllowId, userId },
    } = req
    try {
        let TrustDones = await TrustDone.findOne({ 'trustAllowId': ObjectId(trustAllowId), 'ratedBy': ObjectId(userId), isActive: true }).populate('trustAllowId').populate('ratedBy');
        res.status(200).send({ success: true, data: TrustDones });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res)