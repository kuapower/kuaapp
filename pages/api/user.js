import createHandler from '../../api/middleware/createhandler';
import { userType } from '../../common/enum';
import Company from '../../api/models/CompanyModel';
import Users from '../../api/models/UserModel';
import { generatePassword } from '../../common/global';
const handler = createHandler();
import { sendEmail } from '../../common/emailSend';
import { ObjectId } from 'mongodb';
import config from '../../config';
//Create user
handler.post(async (req, res) => {
    try {
        if (req.body._id == undefined) {
            // let userExist = await Users.findOne({ email: req.body.email, companyId: req.body.companyId, userType: req.body.userType, isActive: true });
            // if (!userExist) {
            let userEmailExist = await Users.findOne({ email: req.body.email, isActive: true }).lean();
            if (!userEmailExist) {
                let password = generatePassword();
                //Send email
                var emailSubject = "Welcome to KUA Power";
                var emailContent = "<html lang='en'><head>" +
                    "<style type='text/css'>" +
                    "@media screen {" +
                    "@font-face {" +
                    "font-family: 'Annie Use Your Telescope';" +
                    "font-style: normal;" +
                    "font-weight: 400;" +
                    "font-display: swap;" +
                    "src: url(https://fonts.gstatic.com/s/annieuseyourtelescope/v11/daaLSS4tI2qYYl3Jq9s_Hu74xwktnlKxH6osGVGTkz3A_0YFZQ.woff2) format('woff2');" +
                    "unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;" +
                    "}" +
                    "body {" +
                    "margin: 0;" +
                    "padding: 0;" +
                    "font-family: 'Annie Use Your Telescope';" +
                    "}" +
                    "}" +
                    "</style></head>" +
                    "<body><div><table><tr>" +
                    "<td style='padding: 20px; font-size: 16px; line-height: 20px;'>" +
                    "<p>Welcome to KUA Power members area!</p>" +
                    "<p>" +
                    "You can access at " + "<a style='text-decoration:none;color:#35B7E8;' href='" + config.websiteHost + "'>" + config.websiteHostWithouthttp + "</a> with your email and following password: ##password##" +
                    "</p>" +
                    "<p>" +
                    "<br />If you have trouble signing in, please send us a message at <a style='text-decoration:none;color:#35B7E8;' href='mailto:" + config.contactemail + "'>" + config.contactemail + "</a>." +
                    "</p>" +
                    "<div style='font-size: 16px; line-height: 30px;'>" +
                    "<span>" +
                    "Thank you!" +
                    "<br />" +
                    "KUA Power team" +
                    "</span>" +
                    "</div>" +
                    "</td>" +
                    "</tr></table></div></body></html>";

                emailContent = emailContent.replace("##password##", password);
                let emailSent = sendEmail(req.body.email, emailSubject, emailContent);
                //if (emailSent == 1) {
                let user = {
                    name: req.body.name,
                    email: req.body.email,
                    pwd: password,
                    companyId: [req.body.companyId],
                    userType: [req.body.userType]
                }
                var newUser = new Users(user);
                var createUser = await newUser.save();
                res.status(200).send({ success: true, data: createUser });
                // }
                // else {
                //     res.status(200).send({ success: false, code: -2, message: 'Something went wrong! Please contact administrator.' });
                // }
            }
            else {
                //Direct create user
                // let user = {
                //     name: req.body.name,
                //     email: req.body.email,
                //     pwd: userEmailExist.pwd,
                //     companyId: req.body.companyId,
                //     userType: req.body.userType
                // }
                // var newUser = new Users(user);
                // var createUser = await newUser.save();


                if (!userEmailExist.companyId.includes(req.body.companyId)) {
                    userEmailExist.companyId.push(req.body.companyId);
                }
                if (!userEmailExist.userType.includes(req.body.userType)) {
                    userEmailExist.userType.push(req.body.userType);
                }

                let userUpdate = await Users.updateOne({ _id: userEmailExist._id }, userEmailExist);

                res.status(200).send({ success: true, data: userUpdate });
            }
            // }
            // else {
            //     res.status(200).send({ success: false, code: -1, message: 'User already exists.' });
            // }
        }
        else {
            let userUpdate = await Users.updateOne({ _id: req.body._id }, req.body);
            res.status(200).send({ success: true, data: userUpdate });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All User
handler.get(async (req, res) => {
    const {
        query: { companyId, userId },
    } = req
    try {
        if (companyId != undefined && userId == undefined) {
            let users = await Users.find({ companyId: companyId, isActive: true, userType: { $ne: userType.superAdmin } }).populate('companyId').lean();
            res.status(200).send({ success: true, data: users });
        }
        else if (userId != undefined && companyId == undefined) {

            let userById = await Users.findById(ObjectId(userId)).lean();
            let users = [];
            for (let i = 0; i < userById.companyId.length; i++) {
                const compId = userById.companyId[i];
                let userTemp = await Users.find({ companyId: ObjectId(compId), isActive: true, userType: { $ne: userType.superAdmin } }).populate('companyId').lean();
                for (let j = 0; j < userTemp.length; j++) {
                    const userT = userTemp[j];
                    if (users.filter(a => a.email == userT.email).length == 0)
                        users.push(userT);
                }
                //users = users.concat(userTemp);
            }
            res.status(200).send({ success: true, data: users });
        }
        else {
            let users = await Users.find({ isActive: true, userType: { $ne: userType.superAdmin } }).populate('companyId').lean();
            res.status(200).send({ success: true, data: users });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Delete user
handler.delete(async (req, res) => {
    try {
        await Users.deleteOne({ _id: req.body.id });
        res.status(200).send({ success: true });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
})

handler.put(async (req, res) => {
    const {
        query: { q, companyId },
    } = req
    try {
        let users = await Users.find({ companyId: companyId, isActive: true, name: { $regex: new RegExp(q, 'i') } }).lean();
        var userArray = users.map(v => ({ value: v._id, label: v.name }));
        res.status(200).send(userArray);
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res)