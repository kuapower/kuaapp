import createHandler from '../../api/middleware/createhandler';
import Users from '../../api/models/UserModel';
const handler = createHandler();

//Post method
handler.post(async (req, res) => {
    try {
        let user = await Users.find({ 'email': req.body.email, 'pwd': req.body.password }).lean();
        if (user.length > 0) {
            let userf = user[0];
            // userf.companyId = [];
            // userf.userType = [];
            // for (let i = 0; i < user.length; i++) {
            //     const us = user[i];
            //     if (userf.companyId.indexOf("" + us.companyId) > -1)
            //         userf.companyId.push("" + us.companyId);
            //     if (userf.userType.indexOf("" + us.userType) > -1)
            //         userf.userType.push("" + us.userType);
            // }
            // console.log("userf", userf);
            res.status(200).send({ success: true, auth: true, user: userf, message: 'Login successful.' });
        }
        else {
            res.status(200).send({ success: false, code: -1, message: 'Invalid password or username.' });
        }
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

// //Get Method
// handler.get(async (req, res) => {
//     try {
//         res.status(200).send({ success: true, auth: true, message: 'Login successful.' });
//     }
//     catch (error) {
//         console.log("Error", error);
//         res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
//     }
// });

export default (req, res) => handler.run(req, res) 