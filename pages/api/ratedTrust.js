import createHandler from '../../api/middleware/createhandler';
import Trusts from '../../api/models/TrustModel';
import Users from '../../api/models/UserModel';
import { ObjectId } from 'mongodb';
const handler = createHandler();

handler.get(async (req, res) => {
    const {
        query: { companyId, ratedByUserID, isPast, currentSessionTime },
    } = req
    try {
        let trusts = [];
        let users = await Users.find({ companyId: companyId });
        var userArray = users.map(function (item) {
            return item['_id'];
        });
        if (isPast == 'undefined' && currentSessionTime == 'undefined') {
            var check = userArray.filter(word => word == ratedByUserID);
            for (let i = 0; i < check.length; i++) {
                let a = check[i];
                trusts = await Trusts.find({ trustByUserId: { $in: a }, isActive: true });
                for (let j = 0; j < trusts.length; j++) {
                    var trust = trusts[j].trustByUserId;
                }
            }
            console.log(trust);
        }

    
        else if (isPast && currentSessionTime != undefined) {
            return;
        }
        res.status(200).send({ success: true, data: trust });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res)