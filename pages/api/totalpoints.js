import createHandler from '../../api/middleware/createhandler';
import Trusts from '../../api/models/TrustModel';
import Users from '../../api/models/UserModel';
import { ObjectId } from 'mongodb';
const handler = createHandler();


handler.get(async (req, res) => {
    const {
        query: { companyId, totalPointsByUserID, isPast, currentSessionTime, loginType}
    } = req
    try {
        var trusts = null;
        let users = await Users.find({ companyId: companyId });
        var userArray = users.map(function (item) {
            return item['_id'];
        });
        
        var adminUsers = await Users.find({ userType: 1 });
        var adminUserArray = adminUsers.map(function (item) {
            return item['_id'];
        })

        if (isPast == 'undefined' && currentSessionTime == 'undefined' && (loginType == "2")) {
            var check = userArray.filter(word => word != totalPointsByUserID && word != `${adminUserArray}`);
            var tPoints = [];
            let totalPoints = 0;
            for (let i = 0; i < check.length; i++) {
                let a = check[i];
                trusts = await Trusts.find({ userId: { $in: a }, isActive: true });
                let totalTrustPoint = 0;
                let totalNumberOfPointWeGot = 0;
                let finaltotalNumberOfPointWeGot = 0;
                for (let j = 0; j < trusts.length; j++) {
                    const trust = trusts[j];
                    totalNumberOfPointWeGot = trusts.length;
                    totalTrustPoint += Number(trust.tValue);
                }
                finaltotalNumberOfPointWeGot = totalNumberOfPointWeGot / 8;
                totalPoints += totalTrustPoint;
                tPoints.push({key:a,value: totalTrustPoint,finaltotalNumberOfPointWeGot:finaltotalNumberOfPointWeGot}); 
            }   
        }
        else if (isPast == 'undefined' && currentSessionTime == 'undefined' && (loginType == "3")) {
            // var check = userArray.filter(word => word != totalPointsByUserID);
            var check = userArray.filter(word => word != totalPointsByUserID && word != `${adminUserArray}`);
            var tPoints = [];
            let totalPoints = 0;
            for (let i = 0; i < check.length; i++) {
                let a = check[i];
                trusts = await Trusts.find({ userId: { $in: a }, isActive: true });
                let totalTrustPoint = 0;
                let totalNumberOfPointWeGot = 0;
                let finaltotalNumberOfPointWeGot = 0;
                for (let j = 0; j < trusts.length; j++) {
                    const trust = trusts[j];
                    totalNumberOfPointWeGot = trusts.length;
                    totalTrustPoint += Number(trust.tValue);
                }
                finaltotalNumberOfPointWeGot = totalNumberOfPointWeGot / 8;
                totalPoints += totalTrustPoint;
                tPoints.push({key:a,value: totalTrustPoint,finaltotalNumberOfPointWeGot:finaltotalNumberOfPointWeGot}); 
            }   
        }
        else if (isPast == 'undefined' && currentSessionTime == 'undefined' && (loginType == "1")) {
            var check = userArray.filter(word => word != totalPointsByUserID);
            var tPoints = [];
            let totalPoints = 0;

            for (let i = 0; i < check.length; i++) {
                let a = check[i];
                trusts = await Trusts.find({ userId: { $in: a }, isActive: true });
                let totalTrustPoint = 0;
                let totalNumberOfPointWeGot = 0;
                let finaltotalNumberOfPointWeGot = 0;
                for (let j = 0; j < trusts.length; j++) {
                    const trust = trusts[j];
                    totalNumberOfPointWeGot = trusts.length;
                    totalTrustPoint += Number(trust.tValue);
                }
                finaltotalNumberOfPointWeGot = totalNumberOfPointWeGot / 8;
                totalPoints += totalTrustPoint;
                tPoints.push({key:a,value: totalTrustPoint,finaltotalNumberOfPointWeGot:finaltotalNumberOfPointWeGot}); 
            }   
        }
        else if (isPast && currentSessionTime != undefined) {
            trusts = await Trusts.find({ 'userId': { $in: userArray }, isActive: true }).populate('userId');
            trusts = trusts.filter(a => Date(a.sessionDateTime) < Date(currentSessionTime));
            console.log("error here check");
        }
        res.status(200).send({ success: true, data: tPoints });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res)