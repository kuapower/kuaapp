import createHandler from '../../api/middleware/createhandler';
import TrustAllow from '../../api/models/TrustAllowModel';
import { ObjectId } from 'mongodb';
const handler = createHandler();

//Create TrustAllow
handler.post(async (req, res) => {
    try {
        var newTrustAllow = new TrustAllow(req.body);
        var created = await newTrustAllow.save();
        res.status(200).send({ success: true, data: created });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

//Get All TrustAllow
handler.get(async (req, res) => {
    const {
        query: { companyId },
    } = req
    try {
        let trustAllows = await TrustAllow.find({ 'companyId': ObjectId(companyId), isActive: true }, {}, { sort: { dateAdded: -1 } }).populate('companyId').populate('ratingAllowedBy').lean();
        res.status(200).send({ success: true, data: trustAllows[0] });
    }
    catch (error) {
        console.log("Error", error);
        res.status(200).send({ success: false, code: -100, message: 'Something went wrong! Please try again.' });
    }
});

export default (req, res) => handler.run(req, res)