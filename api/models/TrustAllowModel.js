import mongoose, { Schema } from 'mongoose';

const MODEL_NAME = 'TrustAllow';

var schema = new Schema({
    companyId: {
        type: Schema.Types.ObjectId,
        ref: "company"
    },
    ratingAllowedBy: {
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    isActive: {
        type: Boolean,
        default: true
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;