import mongoose, { Schema } from 'mongoose';
//import { sgrade } from './common/enum';
const sgrade = {
    A: 1,
    B: 2,
    C: 3,
    tA: 4
};
const MODEL_NAME = 'SeatPer';

var schema = new Schema({
    seatId: {
        type: Schema.Types.ObjectId,
        ref: "Seat"
    },
    // userId: {
    //     type: Schema.Types.ObjectId,
    //     ref: "Users"
    // },
    username: {
        type: String
    },
    grade: {
        type: Number,
        default: sgrade.A
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdBy: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;