import mongoose, { Schema } from 'mongoose';
const MODEL_NAME = 'company';

var schema = new Schema({
    name: {
        type: String,
    },
    score: {
        type: Number
    },
    seatScore: {
        type: Number
    },
    energyScore: {
        type: Number
    },
    trustScore: {
        type: Number
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdBy: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;