import mongoose, { Schema } from 'mongoose';

const MODEL_NAME = 'TrustDone';

var schema = new Schema({
    trustAllowId: {
        type: Schema.Types.ObjectId,
        ref: "TrustAllow"
    },
    ratedBy: {
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    isActive: {
        type: Boolean,
        default: true
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;