import mongoose, { Schema } from 'mongoose';
const MODEL_NAME = 'Activity';

var schema = new Schema({
    name: {
        type: String,
    },
    description: {
        type: String,
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdBy: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;