import mongoose, { Schema } from 'mongoose';
//const { userType } = require('./common/enum');

const userType = {
    superAdmin: 1,
    cAdmin: 2,
    teamMember: 3
};


const MODEL_NAME = 'Users';

var schema = new Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
    },
    pwd: {
        type: String,
    },
    companyId: [{
        type: Schema.Types.ObjectId,
        ref: "company"
    }],
    userType: [{
        type: Number,
        default: userType.teamMember
    }],
    isActive: {
        type: Boolean,
        default: true
    },
    createdBy: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;