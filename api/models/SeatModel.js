import mongoose, { Schema } from 'mongoose';
const MODEL_NAME = 'Seat';

var schema = new Schema({
    companyId: {
        type: Schema.Types.ObjectId,
        ref: "company"
    },
    position: {
        type: String,
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    description: {
        type: String,
    },
    parent: {
        type: Number
    },
    parentSeatId: {
        type: String,
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdBy: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;