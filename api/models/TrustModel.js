import mongoose, { Schema } from 'mongoose';

const MODEL_NAME = 'Trust';

var schema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    trustByUserId: {
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    sessionDateTime: {
        type: String,
    },
    type: {
        type: String
    },
    tValue: {
        type: Number
    },
    description: {
        type: String,
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdBy: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    },
    companyId: {
        type: Schema.Types.ObjectId,
        ref: "company"
    },
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;