import mongoose, { Schema } from 'mongoose';
//const { activityType } = require('./common/enum');

const activityType = {
    energizing: 1,
    ok: 2,
    draining: 3
};

const MODEL_NAME = 'Energy';

var schema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    activityId: {
        type: Schema.Types.ObjectId,
        ref: "Activity"
    },
    type: {
        type: Number,
        default: activityType.energizing
    },
    description: {
        type: String,
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdBy: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Model = mongoose.models[MODEL_NAME] || mongoose.model(MODEL_NAME, schema);

export default Model;