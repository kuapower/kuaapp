import mongoose from 'mongoose';
import config from '../../config';
const MONGODB_CONN_STR = config.database;

const databaseMiddleware = async (req, res, next) => {
    try {
        if (!global.mongoose) {
            global.mongoose = await mongoose.connect(MONGODB_CONN_STR, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
            });
        }
    }
    catch (ex) {
        console.error(ex);
    }
    return next();
};
export default databaseMiddleware;

// const middleware = nextConnect();
// middleware.use(database);
// export default middleware;

// import { MongoClient } from 'mongodb';
// import nextConnect from 'next-connect';
// import config from '../config';

// const client = new MongoClient(config.database, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
// });
// async function database(req, res, next) {
//     if (!client.isConnected()) await client.connect();
//     req.dbClient = client;
//     req.db = client.db('kuapower');
//     return next();
// }
// const middleware = nextConnect();

// middleware.use(database);

// export default middleware;